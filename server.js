const express = require('express');
const { Pool } = require('pg');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
const PORT = process.env.PORT || 3000;

app.use(cors());
app.use(bodyParser.json());

const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'postgres',  
  password: 'medicx',
  port: 5432,
});


/*
const pool = new Pool({
  user: 'postgres',
  host: '192.168.0.22',  
  database: 'postgres',
  password: 'medicx',
  port: 5432,
});
*/




//POST






// Ruta de registro de usuario
app.post('/api/register', async (req, res) => {
  console.log('Solicitud de registro recibida');
  console.log(req.body);
  try {
    const { nombre, primer_apellido, segundo_apellido, num_colegiado, correo_electronico, contrasena, nivel_acceso } = req.body;

    // Validar que el nivel de acceso sea uno de los valores permitidos
    const allowedAccessLevels = ["Administrador", "Medico", "Enfermera", "Auxiliar_de_enfermer", "Empleado"];
    if (!allowedAccessLevels.includes(nivel_acceso)) {
      return res.status(400).json({ message: 'Nivel de acceso no válido. Debe ser "Administrador", "Medico", "Enfermera", "Auxiliar_de_enfermer" o "Empleado".' });
    }
    console.log('pasa el filtro de nivel de acceso');

    // Comprobar si el usuario ya existe en la base de datos
    const existingUser = await pool.query('SELECT * FROM Usuarios WHERE correo_electronico = $1', [correo_electronico]);

    if (existingUser.rows.length > 0) {
      return res.status(400).json({ message: 'El usuario ya existe' });
    }

    // Validar el número de colegiado nacional
    if (!validarNumeroColegiadoNacional(num_colegiado)) {
      return res.status(400).json({ message: 'Número de colegiado nacional no válido' });
    }

    // Buscar el ID correspondiente al nivel de acceso en la tabla nivel_acceso
    const nivelAccesoResult = await pool.query('SELECT id FROM nivel_acceso WHERE nombre = $1', [nivel_acceso]);

    if (nivelAccesoResult.rows.length === 0) {
      return res.status(400).json({ message: 'Nivel de acceso no encontrado en la tabla nivel_acceso.' });
    }

    const nivelAccesoID = nivelAccesoResult.rows[0].id;

    // Si no existe, inserta un nuevo usuario en la base de datos
    await pool.query('INSERT INTO Usuarios (nombre, primer_apellido, segundo_apellido, num_colegiado, correo_electronico, contrasena, nivel_acceso) VALUES ($1, $2, $3, $4, $5, $6, $7)',
      [nombre, primer_apellido, segundo_apellido, num_colegiado, correo_electronico, contrasena, nivelAccesoID]);

    res.status(201).json({ message: 'Usuario registrado con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Ruta de inicio de sesión usuarios  
app.post('/api/login/usuarios', async (req, res) => {
  console.log('Solicitud de inicio usuario recibida');
  console.log(req.body);

  try {
    const { correo_electronico , contrasena } = req.body;

    // Busca el usuario en la base de datos por su nombre de usuario
    const user = await pool.query('SELECT * FROM usuarios WHERE correo_electronico = $1', [correo_electronico]);

    if (user.rows.length === 0) {
      return res.status(400).json({ message: 'El usuario no existe' });
    }

    // Compara la contraseña proporcionada con la contraseña almacenada
    if (user.rows[0].contrasena !== contrasena) {
      return res.status(401).json({ message: 'Contraseña incorrecta' });
    }
    
    const userRoleQuery = await pool.query('SELECT nivel_acceso FROM Usuarios WHERE correo_electronico = $1', [correo_electronico]);

    if (userRoleQuery.rows.length === 0) {
      return res.status(401).json({ message: 'Usuario no encontrado' });
    }

    const userRole = userRoleQuery.rows[0].nivel_acceso;

    // Devuelve el rol como parte de la respuesta de inicio de sesión
    res.status(200).json({ message: 'Inicio de sesión exitoso', role: userRole });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Ruta de inicio de sesión cliente 
app.post('/api/login/cliente', async (req, res) => {
  console.log('Solicitud de inicio cliente recibida');

  console.log(req.body);

  try {
    const { correo_electronico , contrasena } = req.body;

    // Busca el usuario en la base de datos por su nombre de usuario
    const user = await pool.query('SELECT * FROM cliente WHERE correo_electronico = $1', [correo_electronico]);

    if (user.rows.length === 0) {
      return res.status(400).json({ message: 'El usuario no existe' });
    }

    // Compara la contraseña proporcionada con la contraseña almacenada
    if (user.rows[0].contrasena !== contrasena) {
      return res.status(401).json({ message: 'Contraseña incorrecta' });
    }

    // Inicio de sesión exitoso
    res.status(200).json({ message: 'Inicio de sesión exitoso' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Ruta de registro de cliente
app.post('/api/register_cliente', async (req, res) => {
  console.log('Solicitud de registro de cliente recibida');
  console.log(req.body);
  try {
    const { dni, correo_electronico, contrasena, nombre, primer_apellido, segundo_apellido, fecha_nacimiento, genero, telefono, direccion, codigo_postal } = req.body;

    // Comprobar si el cliente ya existe en la base de datos
    const existingCliente = await pool.query('SELECT * FROM cliente WHERE dni = $1', [dni]);

    if (existingCliente.rows.length > 0) {
      return res.status(400).json({ message: 'El cliente ya existe' });
    }

    // Si no existe, inserta un nuevo cliente en la base de datos
    await pool.query('INSERT INTO cliente (dni, correo_electronico, contrasena, nombre, primer_apellido, segundo_apellido, fecha_nacimiento, genero, telefono, direccion, codigo_postal) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)',
      [dni, correo_electronico, contrasena, nombre, primer_apellido, segundo_apellido, fecha_nacimiento, genero, telefono, direccion, codigo_postal]);

    res.status(201).json({ message: 'Cliente registrado con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});



app.post('/api/cliente_medicamentos', async (req, res) => {
  console.log('Solicitud POST recibida en /api/cliente_medicamentos');

  try {
    // Obtén los datos del cuerpo de la solicitud
    const { cliente_id, usuario, nrregistro, dosis, fecha_ini, fecha_fin, tratamiento, diagnostico, notas, vacuna  } = req.body;

    console.log('Datos recibidos:', req.body);

     // Verifica que todos los campos estén definidos y no sean nulos o undefined
      if (!cliente_id || !usuario || !nrregistro || !dosis || !fecha_ini || !fecha_fin ||
      !tratamiento || !diagnostico || !notas || !vacuna) {
    return res.status(400).json({ message: 'Todos los campos deben estar completos.' });
  }

    const dniValidationResult = ValidateSpanishID(cliente_id);

    if (!dniValidationResult.valid) {
      return res.status(400).json({ message: 'El formato del DNI no es válido.' });
    }

    // Realiza una consulta para obtener el ID del medicamento a partir del nrregistro
    const getMedicamentoIdQuery = 'SELECT id FROM Medicamento WHERE nrregistro = $1;';
    const medicamentoResult = await pool.query(getMedicamentoIdQuery, [nrregistro]);

    if (medicamentoResult.rows.length === 0) {
      return res.status(400).json({ message: 'El medicamento con el nrregistro proporcionado no existe.' });
    }

    const medicamento_id = medicamentoResult.rows[0].id;

 // Realiza una consulta para obtener el ID del tratamiento a partir del nombre del tratamiento
  const getTratamientoIdQuery = 'SELECT id FROM Tratamiento WHERE descripcion = $1;';
  const tratamientoResult = await pool.query(getTratamientoIdQuery, [tratamiento]);

  if (tratamientoResult.rows.length === 0) {
    return res.status(400).json({ message: 'El tratamiento proporcionado no existe.' });
  }

  const tratamiento_id = tratamientoResult.rows[0].id;

// Realiza una consulta para obtener el ID del diagnóstico a partir del nombre del diagnóstico
  const getDiagnosticoIdQuery = 'SELECT id FROM Diagnostico WHERE descripcion = $1;';
  const diagnosticoResult = await pool.query(getDiagnosticoIdQuery, [diagnostico]);

  if (diagnosticoResult.rows.length === 0) {
    return res.status(400).json({ message: 'El diagnóstico proporcionado no existe.' });
  }

  const diagnostico_id = diagnosticoResult.rows[0].id;

   // Realiza una consulta para obtener el ID de la vacuna a partir del nombre de la vacuna
  const getVacunaIdQuery = 'SELECT id FROM vacunas WHERE nombre = $1;';
  const vacunaResult = await pool.query(getVacunaIdQuery, [vacuna]);

  if (vacunaResult.rows.length === 0) {
    return res.status(400).json({ message: 'La vacuna proporcionada no existe.' });
  }

  const vacuna_id = vacunaResult.rows[0].id;


    const getNumColegiadoQuery = 'SELECT id_usuarios FROM usuarios WHERE correo_electronico = $1;';
    const numColegiadoResult = await pool.query(getNumColegiadoQuery, [usuario]);
    
    if (numColegiadoResult.rows.length === 0) {
      return res.status(400).json({ message: 'No se encontró el id_usuarios para el usuario proporcionado.' });
    }
    
    const usuario_id = numColegiadoResult.rows[0].id_usuarios; 

    const fecha = new Date().toISOString().split('T')[0];

    // Realiza la lógica para insertar el nuevo registro en la base de datos
    const insertQuery = `
      INSERT INTO Cliente_Medicamentos 
      (cliente_id, usuario_id, medicamento_id, dosis, fecha_ini, fecha_fin) 
      VALUES ($1, $2, $3, $4, $5, $6) RETURNING id;
    `;
    const values = [cliente_id, usuario_id, medicamento_id, dosis, fecha_ini, fecha_fin];

    const { rows } = await pool.query(insertQuery, values);
    const nuevoIdDelRegistro = rows[0].id1;


    // Realiza la lógica para insertar el nuevo registro en la base de datos
    const insertQueryH = `
      INSERT INTO historial_medico 
      (cliente_id, fecha, vacuna_id, diagnostico_id, tratamiento_id, notas, usuario_id) 
      VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id;
    `;
    const valuesH = [cliente_id, fecha, vacuna_id, diagnostico_id, tratamiento_id, notas, usuario_id];

    console.log('valuesH:', valuesH);
    console.log('insertQueryH:', insertQueryH);

    const resultH = await pool.query(insertQueryH, valuesH);
    console.log('Result H:', resultH);
    const { rows: rowsH } = resultH;


      // Verifica si rowsH está definido y tiene al menos un elemento
  if (rowsH && rowsH.length > 0) {
    const nuevoIdDelRegistroH = rowsH[0].id;

    // Puedes utilizar nuevoIdDelRegistroH aquí según tus necesidades

    // Envía una respuesta al cliente con el ID del nuevo registro
    res.status(201).json({ message: 'Registro creado con éxito de historial_medico y de ', id: nuevoIdDelRegistroH });
  } else {
    console.error('No se obtuvieron resultados al insertar en historial_medico.');
    res.status(500).json({ message: 'Error en el servidor', error: 'No se obtuvieron resultados al insertar en historial_medico.' });
  }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para insertar vacunas
app.post('/api/vacunas', async (req, res) => {
  console.log('Solicitud POST recibida en /api/vacunas');

  try {
    // Obtén los datos del cuerpo de la solicitud
    const { nombre, descripcion } = req.body;

    console.log('Datos recibidos:', req.body);

    // Verifica que todos los campos estén definidos y no sean nulos o undefined
    if (!nombre || !descripcion) {
      return res.status(400).json({ message: 'Todos los campos deben estar completos.' });
    }

    // Obtiene el último ID en la tabla y le suma 1 para obtener el nuevo ID
    const getLastIdQuery = 'SELECT MAX(id) + 1 AS new_id FROM vacunas;';
    const { rows: lastIdResult } = await pool.query(getLastIdQuery);
    const nuevoIdDelRegistro = lastIdResult[0].new_id || 1; // Si la tabla está vacía, comenzamos desde 1

    // Verifica si el nombre de la vacuna ya existe en la base de datos
    const checkExistingVacunaQuery = 'SELECT id FROM vacunas WHERE nombre = $1;';
    const existingVacunaResult = await pool.query(checkExistingVacunaQuery, [nombre]);

    if (existingVacunaResult.rows.length > 0) {
      return res.status(400).json({ message: 'Ya existe una vacuna con este nombre.' });
    }

    // Realiza la lógica para insertar el nuevo registro en la tabla de vacunas
    const insertVacunaQuery = `
      INSERT INTO vacunas (id, nombre, descripcion) 
      VALUES ($1, $2, $3) RETURNING id;
    `;
    const values = [nuevoIdDelRegistro, nombre, descripcion];

    const { rows } = await pool.query(insertVacunaQuery, values);

    // Envía una respuesta al cliente con el ID del nuevo registro
    res.status(201).json({ message: 'Registro de vacuna creado con éxito', id: nuevoIdDelRegistro });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para insertar diagnósticos
app.post('/api/diagnosticos', async (req, res) => {
  console.log('Solicitud POST recibida en /api/diagnosticos');

  try {
    // Obtén los datos del cuerpo de la solicitud
    const { descripcion } = req.body;

    console.log('Datos recibidos:', req.body);

    // Verifica que todos los campos estén definidos y no sean nulos o undefined
    if (!descripcion) {
      return res.status(400).json({ message: 'Todos los campos deben estar completos.' });
    }

    // Obtiene el último ID en la tabla y le suma 1 para obtener el nuevo ID
    const getLastIdQuery = 'SELECT MAX(id) + 1 AS new_id FROM Diagnostico;';
    const { rows: lastIdResult } = await pool.query(getLastIdQuery);
    const nuevoIdDelRegistro = lastIdResult[0].new_id || 1; // Si la tabla está vacía, comenzamos desde 1

    // Realiza la lógica para insertar el nuevo registro en la tabla de diagnósticos
    const insertDiagnosticoQuery = `
      INSERT INTO Diagnostico (id, descripcion) 
      VALUES ($1, $2) RETURNING id;
    `;
    const values = [nuevoIdDelRegistro, descripcion];

    const { rows } = await pool.query(insertDiagnosticoQuery, values);

    // Envía una respuesta al cliente con el ID del nuevo registro
    res.status(201).json({ message: 'Registro de diagnóstico creado con éxito', id: nuevoIdDelRegistro });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para insertar tratamientos
app.post('/api/tratamientos', async (req, res) => {
  console.log('Solicitud POST recibida en /api/tratamientos');

  try {
    // Obtén los datos del cuerpo de la solicitud
    const { descripcion } = req.body;

    console.log('Datos recibidos:', req.body);

    // Verifica que todos los campos estén definidos y no sean nulos o undefined
    if (!descripcion) {
      return res.status(400).json({ message: 'Todos los campos deben estar completos.' });
    }

    // Obtiene el último ID en la tabla y le suma 1 para obtener el nuevo ID
    const getLastIdQuery = 'SELECT MAX(id) + 1 AS new_id FROM Tratamiento;';
    const { rows: lastIdResult } = await pool.query(getLastIdQuery);
    const nuevoIdDelRegistro = lastIdResult[0].new_id || 1; // Si la tabla está vacía, comenzamos desde 1

    // Realiza la lógica para insertar el nuevo registro en la tabla de tratamientos
    const insertTratamientoQuery = `
      INSERT INTO Tratamiento (id, descripcion) 
      VALUES ($1, $2) RETURNING id;
    `;
    const values = [nuevoIdDelRegistro, descripcion];

    const { rows } = await pool.query(insertTratamientoQuery, values);

    // Envía una respuesta al cliente con el ID del nuevo registro
    res.status(201).json({ message: 'Registro de tratamiento creado con éxito', id: nuevoIdDelRegistro });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para insertar niveles de acceso
app.post('/api/niveles_acceso', async (req, res) => {
  console.log('Solicitud POST recibida en /api/niveles_acceso');

  try {
    // Obtén los datos del cuerpo de la solicitud
    const { nombre } = req.body;

    console.log('Datos recibidos:', req.body);

    // Verifica que todos los campos estén definidos y no sean nulos o undefined
    if (!nombre) {
      return res.status(400).json({ message: 'Todos los campos deben estar completos.' });
    }

    // Obtiene el próximo valor de la secuencia asociada a la columna id
    const getNextIdQuery = 'SELECT nextval(\'nivel_acceso_id_seq\') as new_id;';
    const { rows: nextIdResult } = await pool.query(getNextIdQuery);
    const nuevoIdDelRegistro = nextIdResult[0].new_id;

    // Realiza la lógica para insertar el nuevo registro en la tabla de niveles de acceso
    const insertNivelAccesoQuery = `
      INSERT INTO nivel_acceso (id, nombre) 
      VALUES ($1, $2) RETURNING id;
    `;
    const values = [nuevoIdDelRegistro, nombre];

    const { rows } = await pool.query(insertNivelAccesoQuery, values);

    // Envía una respuesta al cliente con el ID del nuevo registro
    res.status(201).json({ message: 'Registro de nivel de acceso creado con éxito', id: nuevoIdDelRegistro });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para insertar fabricantes
app.post('/api/fabricantes', async (req, res) => {
  console.log('Solicitud POST recibida en /api/fabricantes');

  try {
    // Obtén los datos del cuerpo de la solicitud
    const { nombre, pais, paginaweb } = req.body;

    console.log('Datos recibidos:', req.body);

    // Verifica que todos los campos estén definidos y no sean nulos o undefined
    if (!nombre || !pais || !paginaweb) {
      return res.status(400).json({ message: 'Todos los campos deben estar completos.' });
    }

    // Obtiene el último ID en la tabla y le suma 1 para obtener el nuevo ID
    const getLastIdQuery = 'SELECT MAX(id_fabricante) + 1 AS new_id FROM fabricante;';
    const { rows: lastIdResult } = await pool.query(getLastIdQuery);
    const nuevoIdDelRegistro = lastIdResult[0].new_id || 1; // Si la tabla está vacía, comenzamos desde 1

    // Realiza la lógica para insertar el nuevo registro en la tabla de fabricantes
    const insertFabricanteQuery = `
      INSERT INTO fabricante (id_fabricante, nombre, pais, paginaweb) 
      VALUES ($1, $2, $3, $4) RETURNING id_fabricante;
    `;
    const values = [nuevoIdDelRegistro, nombre, pais, paginaweb];

    const { rows } = await pool.query(insertFabricanteQuery, values);

    // Envía una respuesta al cliente con el ID del nuevo registro
    res.status(201).json({ message: 'Registro de fabricante creado con éxito', id: nuevoIdDelRegistro });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para insertar tipos de medicamento
app.post('/api/tiposmedicamento   ', async (req, res) => {
  console.log('Solicitud POST recibida en /api/tiposmedicamento');

  try {
    // Obtén los datos del cuerpo de la solicitud
    const { tipo } = req.body;

    console.log('Datos recibidos:', req.body);

    // Verifica que todos los campos estén definidos y no sean nulos o undefined
    if (!tipo) {
      return res.status(400).json({ message: 'Todos los campos deben estar completos.' });
    }

    // Obtiene el último ID en la tabla y le suma 1 para obtener el nuevo ID
    const getLastIdQuery = 'SELECT MAX(id) + 1 AS new_id FROM tipomedicamento;';
    const { rows: lastIdResult } = await pool.query(getLastIdQuery);
    const nuevoIdDelRegistro = lastIdResult[0].new_id || 1; // Si la tabla está vacía, comenzamos desde 1

    // Realiza la lógica para insertar el nuevo registro en la tabla de tipos de medicamento
    const insertTipoMedicamentoQuery = `
      INSERT INTO tipomedicamento (id, tipo) 
      VALUES ($1, $2) RETURNING id;
    `;
    const values = [nuevoIdDelRegistro, tipo];

    const { rows } = await pool.query(insertTipoMedicamentoQuery, values);

    // Envía una respuesta al cliente con el ID del nuevo registro
    res.status(201).json({ message: 'Registro de tipo de medicamento creado con éxito', id: nuevoIdDelRegistro });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para insertar horarios médicos
app.post('/api/horariosmedicos', async (req, res) => {
  console.log('Solicitud POST recibida en /api/horariosmedicos');

  try {
    // Obtén los datos del cuerpo de la solicitud
    const { num_colegiado, dia_semana, hora_inicio, hora_fin } = req.body;

    console.log('Datos recibidos:', req.body);

    // Verifica que los campos necesarios estén presentes
    if (!num_colegiado || !dia_semana || !hora_inicio || !hora_fin) {
      return res.status(400).json({ message: 'Proporciona el número de colegiado y los demás campos necesarios.' });
    }

    // Realiza una consulta para obtener el ID del médico a partir del número de colegiado
    const getMedicoIdQuery = 'SELECT id_usuarios FROM usuarios WHERE num_colegiado = $1;';
    const medicoIdResult = await pool.query(getMedicoIdQuery, [num_colegiado]);

    if (medicoIdResult.rows.length === 0) {
      return res.status(400).json({ message: 'No se encontró un médico con el número de colegiado proporcionado.' });
    }

    const medico_id = medicoIdResult.rows[0].id_usuarios;

    // Obtiene el último ID en la tabla y le suma 1 para obtener el nuevo ID
    const getLastIdQuery = 'SELECT MAX(id) + 1 AS new_id FROM horarios_medicos;';
    const { rows: lastIdResult } = await pool.query(getLastIdQuery);
    const nuevoIdDelRegistro = lastIdResult[0].new_id || 1; // Si la tabla está vacía, comenzamos desde 1

    // Realiza la lógica para insertar el nuevo registro en la tabla de horarios médicos
    const insertHorarioMedicoQuery = `
      INSERT INTO horarios_medicos (id, medico_id, dia_semana, hora_inicio, hora_fin) 
      VALUES ($1, $2, $3, $4, $5) RETURNING id;
    `;
    const values = [nuevoIdDelRegistro, medico_id, dia_semana, hora_inicio, hora_fin];

    const { rows } = await pool.query(insertHorarioMedicoQuery, values);

    // Envía una respuesta al cliente con el ID del nuevo registro
    res.status(201).json({ message: 'Registro de horario médico creado con éxito', id: nuevoIdDelRegistro });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para insertar resultados de laboratorio
app.post('/api/laboratorio', async (req, res) => {
  console.log('Solicitud POST recibida en /api/laboratorio');

  try {
    // Obtén los datos del cuerpo de la solicitud
    const { paciente_id, tipo_prueba, resultado, fecha_realizacion } = req.body;

    console.log('Datos recibidos:', req.body);

    // Verifica que los campos necesarios estén presentes
    if (!paciente_id || !tipo_prueba || !fecha_realizacion) {
      return res.status(400).json({ message: 'Proporciona el paciente_id, tipo_prueba y fecha_realizacion.' });
    }

    // Realiza una consulta para verificar si el paciente existe
    const checkPacienteQuery = 'SELECT dni FROM cliente WHERE dni = $1;';
    const pacienteResult = await pool.query(checkPacienteQuery, [paciente_id]);

    if (pacienteResult.rows.length === 0) {
      return res.status(400).json({ message: 'No se encontró un paciente con el ID proporcionado.' });
    }

    // Obtiene el último ID en la tabla y le suma 1 para obtener el nuevo ID
    const getLastIdQuery = 'SELECT MAX(id) + 1 AS new_id FROM laboratorio;';
    const { rows: lastIdResult } = await pool.query(getLastIdQuery);
    const nuevoIdDelRegistro = lastIdResult[0].new_id || 1; // Si la tabla está vacía, comenzamos desde 1

    // Realiza la lógica para insertar el nuevo registro en la tabla de laboratorio
    const insertLaboratorioQuery = `
      INSERT INTO laboratorio (id, paciente_id, tipo_prueba, resultado, fecha_realizacion) 
      VALUES ($1, $2, $3, $4, $5) RETURNING id;
    `;
    const values = [nuevoIdDelRegistro, paciente_id, tipo_prueba, resultado, fecha_realizacion];

    const { rows } = await pool.query(insertLaboratorioQuery, values);

    // Envía una respuesta al cliente con el ID del nuevo registro
    res.status(201).json({ message: 'Registro de laboratorio creado con éxito', id: nuevoIdDelRegistro });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para insertar fechas de vacaciones
app.post('/api/fechasvacaciones', async (req, res) => {
  console.log('Solicitud POST recibida en /api/fechasvacaciones');

  try {
    // Obtén los datos del cuerpo de la solicitud
    const { num_colegiado, fecha_inicio_vacaciones, fecha_fin_vacaciones } = req.body;

    console.log('Datos recibidos:', req.body);

    // Verifica que los campos necesarios estén presentes
    if (!num_colegiado || !fecha_inicio_vacaciones || !fecha_fin_vacaciones) {
      return res.status(400).json({ message: 'Proporciona el número de colegiado y las fechas de inicio y fin de vacaciones.' });
    }

    // Realiza una consulta para obtener el ID del médico a partir del número de colegiado
    const getMedicoIdQuery = 'SELECT id_usuarios FROM usuarios WHERE num_colegiado = $1;';
    const medicoIdResult = await pool.query(getMedicoIdQuery, [num_colegiado]);

    if (medicoIdResult.rows.length === 0) {
      return res.status(400).json({ message: 'No se encontró un médico con el número de colegiado proporcionado.' });
    }

    const medico_id = medicoIdResult.rows[0].id_usuarios;

    // Obtiene el último ID en la tabla y le suma 1 para obtener el nuevo ID
    const getLastIdQuery = 'SELECT MAX(id) + 1 AS new_id FROM fechas_vacaciones;';
    const { rows: lastIdResult } = await pool.query(getLastIdQuery);
    const nuevoIdDelRegistro = lastIdResult[0].new_id || 1; // Si la tabla está vacía, comenzamos desde 1

    // Realiza la lógica para insertar el nuevo registro en la tabla de fechas de vacaciones
    const insertFechasVacacionesQuery = `
      INSERT INTO fechas_vacaciones (id, medico_id, fecha_inicio_vacaciones, fecha_fin_vacaciones) 
      VALUES ($1, $2, $3, $4) RETURNING id;
    `;
    const values = [nuevoIdDelRegistro, medico_id, fecha_inicio_vacaciones, fecha_fin_vacaciones];

    const { rows } = await pool.query(insertFechasVacacionesQuery, values);

    // Envía una respuesta al cliente con el ID del nuevo registro
    res.status(201).json({ message: 'Registro de fechas de vacaciones creado con éxito', id: nuevoIdDelRegistro });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para insertar medicamentos
app.post('/api/medicamentos', async (req, res) => {
  console.log('Solicitud POST recibida en /api/medicamentos');

  try {
    // Obtén los datos del cuerpo de la solicitud
    const { nombre, fabricante, tipo, contraindicaciones, nrregistro } = req.body;

    console.log('Datos recibidos:', req.body);

    // Verifica que los campos necesarios estén presentes
    if (!nombre || !fabricante || !tipo) {
      return res.status(400).json({ message: 'Proporciona el nombre, fabricante y tipo del medicamento.' });
    }

    // Realiza una consulta para obtener el ID del fabricante a partir del nombre del fabricante
    const getFabricanteIdQuery = 'SELECT id_fabricante FROM fabricante WHERE nombre = $1;';
    const fabricanteIdResult = await pool.query(getFabricanteIdQuery, [fabricante]);

    if (fabricanteIdResult.rows.length === 0) {
      return res.status(400).json({ message: 'No se encontró un fabricante con el nombre proporcionado.' });
    }

    const id_fabricante = fabricanteIdResult.rows[0].id_fabricante;

    // Realiza una consulta para obtener el ID del tipo de medicamento a partir del nombre del tipo
    const getTipoIdQuery = 'SELECT id FROM tipomedicamento WHERE tipo = $1;';
    const tipoIdResult = await pool.query(getTipoIdQuery, [tipo]);

    if (tipoIdResult.rows.length === 0) {
      return res.status(400).json({ message: 'No se encontró un tipo de medicamento con el nombre proporcionado.' });
    }

    const id_tipo = tipoIdResult.rows[0].id;

    // Obtiene el último ID en la tabla y le suma 1 para obtener el nuevo ID
    const getLastIdQuery = 'SELECT MAX(id) + 1 AS new_id FROM medicamento;';
    const { rows: lastIdResult } = await pool.query(getLastIdQuery);
    const nuevoIdDelRegistro = lastIdResult[0].new_id || 1; // Si la tabla está vacía, comenzamos desde 1

    // Realiza la lógica para insertar el nuevo registro en la tabla de medicamentos
    const insertMedicamentoQuery = `
      INSERT INTO medicamento (id, nombre, id_fabricante, id_tipo, contraindicaciones, nrregistro) 
      VALUES ($1, $2, $3, $4, $5, $6) RETURNING id;`;
    const values = [nuevoIdDelRegistro, nombre, id_fabricante, id_tipo, contraindicaciones, nrregistro];

    const { rows } = await pool.query(insertMedicamentoQuery, values);

    // Envía una respuesta al cliente con el ID del nuevo registro
    res.status(201).json({ message: 'Registro de medicamento creado con éxito', id: nuevoIdDelRegistro });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para insertar cliente_medicamentos
app.post('/api/cliente_medicamentos', async (req, res) => {
  console.log('Solicitud POST recibida en /api/cliente_medicamentos');

  try {
    // Obtén los datos del cuerpo de la solicitud
    const { cliente_id, usuario, medicamento, dosis, fecha_ini, fecha_fin } = req.body;

    console.log('Datos recibidos:', req.body);

    // Verifica que los campos necesarios estén presentes
    if (!cliente_id || !usuario || !medicamento || !dosis || !fecha_ini || !fecha_fin) {
      console.log('Campos faltantes:', { cliente_id, usuario, medicamento, dosis, fecha_ini, fecha_fin });
      return res.status(400).json({ message: 'Todos los campos deben estar completos.' });
    }

    // Realiza una consulta para obtener el ID del medicamento a partir del nombre del medicamento
    const getMedicamentoIdQuery = 'SELECT id FROM medicamento WHERE nombre = $1;';
    const medicamentoIdResult = await pool.query(getMedicamentoIdQuery, [medicamento]);

    if (medicamentoIdResult.rows.length === 0) {
      return res.status(400).json({ message: 'No se encontró un medicamento con el nombre proporcionado.' });
    }

    const medicamento_id = medicamentoIdResult.rows[0].id;

    // Obtiene el último ID en la tabla y le suma 1 para obtener el nuevo ID
    const getLastIdQuery = 'SELECT MAX(id) + 1 AS new_id FROM cliente_medicamentos;';
    const { rows: lastIdResult } = await pool.query(getLastIdQuery);
    const nuevoIdDelRegistro = lastIdResult[0].new_id || 1; // Si la tabla está vacía, comenzamos desde 1

    // Realiza la lógica para insertar el nuevo registro en la tabla de cliente_medicamentos
    const insertClienteMedicamentoQuery = `
      INSERT INTO cliente_medicamentos (id, cliente_id, usuario, medicamento_id, dosis, fecha_ini, fecha_fin) 
      VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id;
    `;
    const values = [nuevoIdDelRegistro, cliente_id, usuario, medicamento_id, dosis, fecha_ini, fecha_fin];

    const { rows } = await pool.query(insertClienteMedicamentoQuery, values);

    // Envía una respuesta al cliente con el ID del nuevo registro
    res.status(201).json({ message: 'Registro de cliente_medicamento creado con éxito', id: nuevoIdDelRegistro });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});































//GETT





app.get('/api/historial_medico', async (req, res) => {
  console.log('Solicitud de todos los historiales médicos recibida');

  try {
    // Realiza una consulta para obtener todos los historiales médicos
    const getAllHistorialMedicoQuery = 'SELECT * FROM historial_medico;';
    const historialMedicoResult = await pool.query(getAllHistorialMedicoQuery);

    // Si no hay datos en el historial médico, devolver un mensaje adecuado
    if (historialMedicoResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron historiales médicos.' });
    }

    // Obtener descripciones asociadas a los IDs de diagnóstico, tratamiento y vacuna
    const idsDiagnostico = historialMedicoResult.rows.map(row => row.diagnostico_id);
    const idsTratamiento = historialMedicoResult.rows.map(row => row.tratamiento_id);
    const idsVacuna = historialMedicoResult.rows.map(row => row.vacuna_id);
    const idsUsuario = historialMedicoResult.rows.map(row => row.usuario_id);

    const getDescripcionQuery = (tableName, ids) => `SELECT id, descripcion FROM ${tableName} WHERE id IN (${ids.join(',')});`;

    const diagnosticoResult = await pool.query(getDescripcionQuery('diagnostico', idsDiagnostico));
    const tratamientoResult = await pool.query(getDescripcionQuery('tratamiento', idsTratamiento));
    const vacunaResult = await pool.query(getDescripcionQuery('vacunas', idsVacuna));

    // Obtener información del cliente
    const clienteIds = historialMedicoResult.rows.map(row => `'${row.cliente_id}'`);
    const getClienteInfoQuery = `SELECT dni as cliente_id, nombre, primer_apellido, segundo_apellido FROM cliente WHERE dni IN (${clienteIds.join(',')});`;
    const clienteInfoResult = await pool.query(getClienteInfoQuery);

    // Crear un mapa para buscar fácilmente la información del cliente por cliente_id
    const clienteInfoMap = {};
    clienteInfoResult.rows.forEach(row => {
      clienteInfoMap[row.cliente_id] = {
        nombre: row.nombre,
        primer_apellido: row.primer_apellido,
        segundo_apellido: row.segundo_apellido,
      };
    });

    // Obtener información del usuario
    const usuarioIds = historialMedicoResult.rows.map(row => row.usuario_id);
    const getUsuarioInfoQuery = `SELECT id_usuarios as usuario_id, nombre, num_colegiado FROM usuarios WHERE id_usuarios IN (${usuarioIds.join(',')});`;
    const usuarioInfoResult = await pool.query(getUsuarioInfoQuery);

    // Crear un mapa para buscar fácilmente la información del usuario por usuario_id
    const usuarioInfoMap = {};
    usuarioInfoResult.rows.forEach(row => {
      usuarioInfoMap[row.usuario_id] = {
        nombre_usuario: row.nombre,
        num_colegiado: row.num_colegiado,
      };
    });

    // Agregar descripciones y datos del cliente y usuario a los historiales médicos
    const historialesConDescripciones = historialMedicoResult.rows.map(row => {
      const clienteInfo = clienteInfoMap[row.cliente_id] || {};
      const usuarioInfo = usuarioInfoMap[row.usuario_id] || {};
      return {
        id: row.id,
        cliente_id: row.cliente_id,
        nombre_cliente: clienteInfo.nombre || '',
        apellidos_cliente: `${clienteInfo.primer_apellido || ''} ${clienteInfo.segundo_apellido || ''}`,
        nombre_usuario: usuarioInfo.nombre_usuario || '',
        num_colegiado_usuario: usuarioInfo.num_colegiado || '', 
        fecha: row.fecha,
        descripcion_diagnostico: diagnosticoResult.rows.find(desc => desc.id === row.diagnostico_id)?.descripcion,
        descripcion_tratamiento: tratamientoResult.rows.find(desc => desc.id === row.tratamiento_id)?.descripcion,
        descripcion_vacuna: vacunaResult.rows.find(desc => desc.id === row.vacuna_id)?.descripcion,
        notas: row.notas,
      };
    });

    // Devuelve todos los historiales médicos con descripciones como parte de la respuesta
    res.status(200).json({ message: 'Historiales médicos obtenidos con éxito', historiales_medicos: historialesConDescripciones });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Ruta para obtener historial médico de un cliente
app.get('/api/historial_medico/:cliente_id', async (req, res) => {
  console.log('Solicitud de historial médico recibida');

  const cliente_id = req.params.cliente_id;
  console.log(`Solicitud de historial médico recibida para el cliente_id: ${cliente_id}`);

  try {
    console.log(`Solicitud de historial médico recibida para el cliente_id dentro del try: ${cliente_id}`);
    // Realiza una consulta para obtener el historial médico del cliente
    const getHistorialMedicoQuery = 'SELECT * FROM historial_medico WHERE cliente_id = $1;';
    const historialMedicoResult = await pool.query(getHistorialMedicoQuery, [cliente_id]);
    console.log(`Solicitud de historial médico recibida para el cliente_id antes del mensaje de error : ${cliente_id}`);
    
    // Si no hay datos en el historial médico, devolver un mensaje adecuado
    if (historialMedicoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Historial médico no encontrado para el cliente proporcionado.' });
    }

    // Obtener descripciones asociadas a los IDs de diagnóstico, tratamiento y vacuna
    const idsDiagnostico = historialMedicoResult.rows.map(row => row.diagnostico_id);
    const idsTratamiento = historialMedicoResult.rows.map(row => row.tratamiento_id);
    const idsVacuna = historialMedicoResult.rows.map(row => row.vacuna_id);

    const getDescripcionQuery = (tableName, ids) => `SELECT id, descripcion FROM ${tableName} WHERE id IN (${ids.join(',')});`;

    const diagnosticoResult = await pool.query(getDescripcionQuery('diagnostico', idsDiagnostico));
    const tratamientoResult = await pool.query(getDescripcionQuery('tratamiento', idsTratamiento));
    const vacunaResult = await pool.query(getDescripcionQuery('vacunas', idsVacuna));

    // Obtener información del cliente
    const clienteIds = historialMedicoResult.rows.map(row => `'${row.cliente_id}'`);
    const getClienteInfoQuery = `SELECT dni as cliente_id, nombre, primer_apellido, segundo_apellido FROM cliente WHERE dni IN (${clienteIds.join(',')});`;
    const clienteInfoResult = await pool.query(getClienteInfoQuery);

    // Crear un mapa para buscar fácilmente la información del cliente por cliente_id
    const clienteInfoMap = {};
    clienteInfoResult.rows.forEach(row => {
      clienteInfoMap[row.cliente_id] = {
        nombre: row.nombre,
        primer_apellido: row.primer_apellido,
        segundo_apellido: row.segundo_apellido,
      };
    });

    // Obtener información del usuario
    const usuarioIds = historialMedicoResult.rows.map(row => row.usuario_id);
    const getUsuarioInfoQuery = `SELECT id_usuarios as usuario_id, nombre, num_colegiado FROM usuarios WHERE id_usuarios IN (${usuarioIds.join(',')});`;
    const usuarioInfoResult = await pool.query(getUsuarioInfoQuery);

    // Crear un mapa para buscar fácilmente la información del usuario por usuario_id
    const usuarioInfoMap = {};
    usuarioInfoResult.rows.forEach(row => {
      usuarioInfoMap[row.usuario_id] = {
        nombre_usuario: row.nombre,
        num_colegiado: row.num_colegiado,
      };
    });

    // Agregar descripciones y datos del cliente y usuario al historial médico
    const historialConDescripciones = historialMedicoResult.rows.map(row => {
      const clienteInfo = clienteInfoMap[row.cliente_id] || {};
      const usuarioInfo = usuarioInfoMap[row.usuario_id] || {};
      return {
        id: row.id,
        cliente_id: row.cliente_id,
        nombre_cliente: clienteInfo.nombre || '',
        apellidos_cliente: `${clienteInfo.primer_apellido || ''} ${clienteInfo.segundo_apellido || ''}`,
        nombre_usuario: usuarioInfo.nombre_usuario || '',
        num_colegiado_usuario: usuarioInfo.num_colegiado || '', 
        fecha: row.fecha,
        descripcion_diagnostico: diagnosticoResult.rows.find(desc => desc.id === row.diagnostico_id)?.descripcion,
        descripcion_tratamiento: tratamientoResult.rows.find(desc => desc.id === row.tratamiento_id)?.descripcion,
        descripcion_vacuna: vacunaResult.rows.find(desc => desc.id === row.vacuna_id)?.descripcion,
        notas: row.notas,
      };
    });

    // Devuelve el historial médico con descripciones como parte de la respuesta
    res.status(200).json({ message: 'Historial médico obtenido con éxito', historial_medico: historialConDescripciones });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});




// Ruta para obtener historial de clientes
app.get('/api/historial_cliente', async (req, res) => {
  console.log('Solicitud de historial de clientes recibida');

  try {
    // Realiza una consulta para obtener el historial de clientes
    const getHistorialClientesQuery = 'SELECT * FROM cliente;';
    const historialClientesResult = await pool.query(getHistorialClientesQuery);

    // Si no hay datos en el historial de clientes, devolver un mensaje adecuado
    if (historialClientesResult.rows.length === 0) {
      return res.status(404).json({ message: 'Historial de clientes no encontrado.' });
    }

    // Devuelve el historial de clientes como parte de la respuesta
    res.status(200).json({ message: 'Historial de clientes obtenido con éxito', historial_clientes: historialClientesResult.rows });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Ruta para obtener historial de clientes por DNI
app.get('/api/historialclientes/:dni', async (req, res) => {
  console.log('Solicitud de historial de clientes recibida');

  const dni = req.params.dni;
  console.log(`Solicitud de historial de clientes recibida para el DNI: ${dni}`);

  try {
    // Realiza una consulta para obtener el historial de clientes por DNI
    const getHistorialClientesQuery = 'SELECT * FROM cliente WHERE dni = $1;';
    const historialClientesResult = await pool.query(getHistorialClientesQuery, [dni]);

    // Si no hay datos en el historial de clientes, devolver un mensaje adecuado
    if (historialClientesResult.rows.length === 0) {
      return res.status(404).json({ message: 'Historial de clientes no encontrado para el DNI proporcionado.' });
    }

    // Devuelve el historial de clientes como parte de la respuesta
    res.status(200).json({ message: 'Historial de clientes obtenido con éxito', historial_clientes: historialClientesResult.rows });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener todos los niveles de acceso
app.get('/api/nivel_acceso', async (req, res) => {
  console.log('Solicitud de todos los niveles de acceso recibida');

  try {
    // Realiza una consulta para obtener todos los niveles de acceso
    const getAllNivelAccesoQuery = 'SELECT * FROM nivel_acceso;';
    const nivelAccesoResult = await pool.query(getAllNivelAccesoQuery);

    // Si no hay datos en el nivel de acceso, devolver un mensaje adecuado
    if (nivelAccesoResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron niveles de acceso.' });
    }

    // Devuelve todos los niveles de acceso como parte de la respuesta
    res.status(200).json({ message: 'Niveles de acceso obtenidos con éxito', niveles_acceso: nivelAccesoResult.rows });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener un nivel de acceso por su ID
app.get('/api/nivel_acceso/:id', async (req, res) => {
  console.log('Solicitud de nivel de acceso por ID recibida');

  const nivel_id = req.params.id;
  console.log(`Solicitud de nivel de acceso recibida para el ID: ${nivel_id}`);

  try {
    // Realiza una consulta para obtener el nivel de acceso por su ID
    const getNivelAccesoByIdQuery = 'SELECT * FROM nivel_acceso WHERE id = $1;';
    const nivelAccesoResult = await pool.query(getNivelAccesoByIdQuery, [nivel_id]);

    // Si no hay datos para el nivel de acceso, devolver un mensaje adecuado
    if (nivelAccesoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Nivel de acceso no encontrado para el ID proporcionado.' });
    }

    // Devuelve el nivel de acceso encontrado como parte de la respuesta
    res.status(200).json({ message: 'Nivel de acceso obtenido con éxito', nivel_acceso: nivelAccesoResult.rows[0] });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener un nivel de acceso por su nombre
app.get('/api/nivel_acceso/nombre/:nombre', async (req, res) => {
  console.log('Solicitud de nivel de acceso por nombre recibida');

  const nombre_nivel = req.params.nombre;
  console.log(`Solicitud de nivel de acceso recibida para el nombre: ${nombre_nivel}`);

  try {
    // Realiza una consulta para obtener el nivel de acceso por su nombre
    const getNivelAccesoByNombreQuery = 'SELECT * FROM nivel_acceso WHERE nombre = $1;';
    const nivelAccesoResult = await pool.query(getNivelAccesoByNombreQuery, [nombre_nivel]);

    // Si no hay datos para el nivel de acceso, devolver un mensaje adecuado
    if (nivelAccesoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Nivel de acceso no encontrado para el nombre proporcionado.' });
    }

    // Devuelve el nivel de acceso encontrado como parte de la respuesta
    res.status(200).json({ message: 'Nivel de acceso obtenido con éxito', nivel_acceso: nivelAccesoResult.rows[0] });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});



// Endpoint para obtener todos los tratamientos
app.get('/api/tratamientos', async (req, res) => {
  console.log('Solicitud de todos los tratamientos recibida');

  try {
      // Realiza una consulta para obtener todos los tratamientos
      const getAllTratamientosQuery = 'SELECT * FROM tratamiento;';
      const tratamientosResult = await pool.query(getAllTratamientosQuery);

      // Si no hay datos en tratamientos, devolver un mensaje adecuado
      if (tratamientosResult.rows.length === 0) {
          return res.status(404).json({ message: 'No se encontraron tratamientos.' });
      }

      // Devuelve todos los tratamientos como parte de la respuesta
      res.status(200).json({ message: 'Tratamientos obtenidos con éxito', tratamientos: tratamientosResult.rows });
  } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener un tratamiento por su ID
app.get('/api/tratamientos/:id', async (req, res) => {
  console.log('Solicitud de tratamiento por ID recibida');

  const tratamiento_id = req.params.id;
  console.log(`Solicitud de tratamiento recibida para el ID: ${tratamiento_id}`);

  try {
    // Realiza una consulta para obtener el tratamiento por su ID
    const getTratamientoByIdQuery = 'SELECT * FROM tratamiento WHERE id = $1;';
    const tratamientoResult = await pool.query(getTratamientoByIdQuery, [tratamiento_id]);

    // Si no hay datos para el tratamiento, devolver un mensaje adecuado
    if (tratamientoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Tratamiento no encontrado para el ID proporcionado.' });
    }

    // Devuelve el tratamiento encontrado como parte de la respuesta
    res.status(200).json({ message: 'Tratamiento obtenido con éxito', tratamiento: tratamientoResult.rows[0] });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});
// Endpoint para obtener un tratamiento por su descripción
app.get('/api/tratamientos/descripcion/:descripcion', async (req, res) => {
  console.log('Solicitud de tratamiento por descripción recibida');

  const descripcion_tratamiento = req.params.descripcion;
  console.log(`Solicitud de tratamiento recibida para la descripción: ${descripcion_tratamiento}`);

  try {
    // Realiza una consulta para obtener el tratamiento por su descripción
    const getTratamientoByDescripcionQuery = 'SELECT * FROM tratamiento WHERE descripcion = $1;';
    const tratamientoResult = await pool.query(getTratamientoByDescripcionQuery, [descripcion_tratamiento]);

    // Si no hay datos para el tratamiento, devolver un mensaje adecuado
    if (tratamientoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Tratamiento no encontrado para la descripción proporcionada.' });
    }

    // Devuelve el tratamiento encontrado como parte de la respuesta
    res.status(200).json({ message: 'Tratamiento obtenido con éxito', tratamiento: tratamientoResult.rows[0] });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});



// Endpoint para obtener todos los diagnósticos
app.get('/api/diagnosticos', async (req, res) => {
  console.log('Solicitud de todos los diagnósticos recibida');

  try {
    // Realiza una consulta para obtener todos los diagnósticos
    const getAllDiagnosticosQuery = 'SELECT * FROM diagnostico;';
    const diagnosticosResult = await pool.query(getAllDiagnosticosQuery);

    // Si no hay datos en diagnósticos, devolver un mensaje adecuado
    if (diagnosticosResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron diagnósticos.' });
    }

    // Devuelve todos los diagnósticos como parte de la respuesta
    res.status(200).json({ message: 'Diagnosticos obtenidos con éxito', diagnosticos: diagnosticosResult.rows });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener un diagnóstico por su ID
app.get('/api/diagnosticos/:id', async (req, res) => {
  console.log('Solicitud de diagnóstico por ID recibida');

  const id_diagnostico = req.params.id;
  console.log(`Solicitud de diagnóstico recibida para el ID: ${id_diagnostico}`);

  try {
    // Realiza una consulta para obtener el diagnóstico por su ID
    const getDiagnosticoByIdQuery = 'SELECT * FROM diagnostico WHERE id = $1;';
    const diagnosticoResult = await pool.query(getDiagnosticoByIdQuery, [id_diagnostico]);

    // Si no hay datos para el diagnóstico, devolver un mensaje adecuado
    if (diagnosticoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Diagnóstico no encontrado para el ID proporcionado.' });
    }

    // Devuelve el diagnóstico encontrado como parte de la respuesta
    res.status(200).json({ message: 'Diagnóstico obtenido con éxito', diagnostico: diagnosticoResult.rows[0] });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener un diagnóstico por su descripción
app.get('/api/diagnosticos/descripcion/:descripcion', async (req, res) => {
  console.log('Solicitud de diagnóstico por descripción recibida');

  const descripcion_diagnostico = req.params.descripcion;
  console.log(`Solicitud de diagnóstico recibida para la descripción: ${descripcion_diagnostico}`);

  try {
    // Realiza una consulta para obtener el diagnóstico por su descripción
    const getDiagnosticoByDescripcionQuery = 'SELECT * FROM diagnostico WHERE descripcion = $1;';
    const diagnosticoResult = await pool.query(getDiagnosticoByDescripcionQuery, [descripcion_diagnostico]);

    // Si no hay datos para el diagnóstico, devolver un mensaje adecuado
    if (diagnosticoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Diagnóstico no encontrado para la descripción proporcionada.' });
    }

    // Devuelve el diagnóstico encontrado como parte de la respuesta
    res.status(200).json({ message: 'Diagnóstico obtenido con éxito', diagnostico: diagnosticoResult.rows[0] });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para obtener todas las vacunas
app.get('/api/vacunas', async (req, res) => {
  console.log('Solicitud de todas las vacunas recibida');

  try {
    // Realiza una consulta para obtener todas las vacunas
    const getAllVacunasQuery = 'SELECT * FROM vacunas;';
    const vacunasResult = await pool.query(getAllVacunasQuery);

    // Si no hay datos en vacunas, devolver un mensaje adecuado
    if (vacunasResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron vacunas.' });
    }

    // Devuelve todas las vacunas como parte de la respuesta
    res.status(200).json({ message: 'Vacunas obtenidas con éxito', vacunas: vacunasResult.rows });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener una vacuna por su ID
app.get('/api/vacunas/:id', async (req, res) => {
  console.log('Solicitud de vacuna por ID recibida');

  const id_vacuna = req.params.id;
  console.log(`Solicitud de vacuna recibida para el ID: ${id_vacuna}`);

  try {
    // Realiza una consulta para obtener la vacuna por su ID
    const getVacunaByIdQuery = 'SELECT * FROM vacunas WHERE id = $1;';
    const vacunaResult = await pool.query(getVacunaByIdQuery, [id_vacuna]);

    // Si no hay datos para la vacuna, devolver un mensaje adecuado
    if (vacunaResult.rows.length === 0) {
      return res.status(404).json({ message: 'Vacuna no encontrada para el ID proporcionado.' });
    }

    // Devuelve la vacuna encontrada como parte de la respuesta
    res.status(200).json({ message: 'Vacuna obtenida con éxito', vacuna: vacunaResult.rows[0] });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener una vacuna por su descripción
app.get('/api/vacunas/descripcion/:descripcion', async (req, res) => {
  console.log('Solicitud de vacuna por descripción recibida');

  const descripcion_vacuna = req.params.descripcion;
  console.log(`Solicitud de vacuna recibida para la descripción: ${descripcion_vacuna}`);

  try {
    // Realiza una consulta para obtener la vacuna por su descripción
    const getVacunaByDescripcionQuery = 'SELECT * FROM vacunas WHERE descripcion = $1;';
    const vacunaResult = await pool.query(getVacunaByDescripcionQuery, [descripcion_vacuna]);

    // Si no hay datos para la vacuna, devolver un mensaje adecuado
    if (vacunaResult.rows.length === 0) {
      return res.status(404).json({ message: 'Vacuna no encontrada para la descripción proporcionada.' });
    }

    // Devuelve la vacuna encontrada como parte de la respuesta
    res.status(200).json({ message: 'Vacuna obtenida con éxito', vacuna: vacunaResult.rows[0] });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener todos los datos de la tabla de fabricantes
app.get('/api/fabricante', async (req, res) => {
  console.log('Solicitud de todos los fabricantes recibida');

  try {
    // Realiza una consulta para obtener todos los fabricantes
    const getAllFabricantesQuery = 'SELECT * FROM fabricante;';
    const fabricantesResult = await pool.query(getAllFabricantesQuery);

    // Si no hay datos en fabricantes, devolver un mensaje adecuado
    if (fabricantesResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron fabricantes.' });
    }

    // Devuelve todos los fabricantes como parte de la respuesta
    res.status(200).json({ message: 'Fabricantes obtenidos con éxito', fabricantes: fabricantesResult.rows });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener un fabricante por su ID
app.get('/api/fabricante/:id', async (req, res) => {
  console.log('Solicitud de un fabricante por su ID recibida');

  const id_fabricante = req.params.id;
  console.log(`Solicitud de fabricante para el ID: ${id_fabricante}`);

  try {
    // Realiza una consulta para obtener un fabricante por su ID
    const getFabricanteByIdQuery = 'SELECT * FROM fabricante WHERE id = $1;';
    const fabricanteResult = await pool.query(getFabricanteByIdQuery, [id_fabricante]);

    // Si no hay datos para ese ID de fabricante, devolver un mensaje adecuado
    if (fabricanteResult.rows.length === 0) {
      return res.status(404).json({ message: 'Fabricante no encontrado para el ID proporcionado.' });
    }

    // Devuelve el fabricante encontrado como parte de la respuesta
    res.status(200).json({ message: 'Fabricante obtenido con éxito', fabricante: fabricanteResult.rows[0] });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener todos los tipos de medicamento
app.get('/api/tipomedicamento', async (req, res) => {
  console.log('Solicitud de todos los tipos de medicamento recibida');

  try {
    // Realiza una consulta para obtener todos los tipos de medicamento
    const getAllTipoMedicamentoQuery = 'SELECT * FROM tipomedicamento;';
    const tipoMedicamentoResult = await pool.query(getAllTipoMedicamentoQuery);

    // Si no hay datos en tipos de medicamento, devolver un mensaje adecuado
    if (tipoMedicamentoResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron tipos de medicamento.' });
    }

    // Devuelve todos los tipos de medicamento como parte de la respuesta
    res.status(200).json({ message: 'Tipos de medicamento obtenidos con éxito', tipos_medicamento: tipoMedicamentoResult.rows });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener un tipo de medicamento por su ID
app.get('/api/tipomedicamento/:id', async (req, res) => {
  const tipoMedicamentoId = req.params.id;
  console.log(`Solicitud de tipo de medicamento con ID: ${tipoMedicamentoId}`);

  try {
    // Realiza una consulta para obtener el tipo de medicamento por su ID
    const getTipoMedicamentoByIdQuery = 'SELECT * FROM tipomedicamento WHERE id = $1;';
    const tipoMedicamentoResult = await pool.query(getTipoMedicamentoByIdQuery, [tipoMedicamentoId]);

    // Si no hay datos para el tipo de medicamento, devolver un mensaje adecuado
    if (tipoMedicamentoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Tipo de medicamento no encontrado para el ID proporcionado.' });
    }

    // Devuelve el tipo de medicamento encontrado como parte de la respuesta
    res.status(200).json({ message: 'Tipo de medicamento obtenido con éxito', tipo_medicamento: tipoMedicamentoResult.rows[0] });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener un tipo de medicamento por su tipo (descripción)
app.get('/api/tipomedicamento/tipo/:tipo', async (req, res) => {
  const tipoMedicamentoTipo = req.params.tipo;
  console.log(`Solicitud de tipo de medicamento con tipo: ${tipoMedicamentoTipo}`);

  try {
    // Realiza una consulta para obtener el tipo de medicamento por su tipo
    const getTipoMedicamentoByTipoQuery = 'SELECT * FROM tipomedicamento WHERE tipo = $1;';
    const tipoMedicamentoResult = await pool.query(getTipoMedicamentoByTipoQuery, [tipoMedicamentoTipo]);

    // Si no hay datos para el tipo de medicamento, devolver un mensaje adecuado
    if (tipoMedicamentoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Tipo de medicamento no encontrado para el tipo proporcionado.' });
    }

    // Devuelve el tipo de medicamento encontrado como parte de la respuesta
    res.status(200).json({ message: 'Tipo de medicamento obtenido con éxito', tipo_medicamento: tipoMedicamentoResult.rows[0] });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});



// Endpoint para obtener todos los usuarios con información del nivel de acceso
app.get('/api/usuarios', async (req, res) => {
  console.log('Solicitud de todos los usuarios recibida');

  try {
    // Realiza una consulta para obtener todos los usuarios con información del nivel de acceso
    const getAllUsuariosQuery = `SELECT u.*, n.nombre AS nombre_nivel_acceso FROM usuarios uINNER JOIN nivel_acceso n ON u.nivel_acceso = n.id; `;
    const usuariosResult = await pool.query(getAllUsuariosQuery);

    // Si no hay datos en usuarios, devolver un mensaje adecuado
    if (usuariosResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron usuarios.' });
    }

    // Mapea el ID de nivel_acceso a su nombre correspondiente
    const nivelAccesoIds = usuariosResult.rows.map(row => row.nivel_acceso);
    const getNivelAccesoByIdQuery = `SELECT id, nombre FROM nivel_acceso WHERE id IN (${nivelAccesoIds.join(',')});`;
    const nivelAccesoResult = await pool.query(getNivelAccesoByIdQuery);

    // Crea un mapa para buscar fácilmente el nombre del nivel de acceso por su ID
    const nivelAccesoMap = {};
    nivelAccesoResult.rows.forEach(row => {
      nivelAccesoMap[row.id] = row.nombre;
    });

    // Actualiza la respuesta con el nombre del nivel de acceso
    usuariosResult.rows.forEach(user => {
      user.nombre_nivel_acceso = nivelAccesoMap[user.nivel_acceso];
    });

    // Devuelve todos los usuarios con información del nivel de acceso como parte de la respuesta
    res.status(200).json({ message: 'Usuarios obtenidos con éxito', usuarios: usuariosResult.rows });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para obtener un usuario por su ID
app.get('/api/usuarios/:id_usuarios', async (req, res) => {
  console.log('Solicitud de usuario por ID recibida');

  const usuario_id = req.params.id_usuarios;
  console.log(`Solicitud de usuario recibida para el ID: ${usuario_id}`);

  try {
    // Realiza una consulta para obtener el usuario por su ID
    const getUsuarioByIdQuery = 'SELECT * FROM usuarios WHERE id_usuarios = $1;';
    const usuarioResult = await pool.query(getUsuarioByIdQuery, [usuario_id]);

    // Si no hay datos para el usuario, devolver un mensaje adecuado
    if (usuarioResult.rows.length === 0) {
      return res.status(404).json({ message: 'Usuario no encontrado para el ID proporcionado.' });
    }

    const nivelAccesoId = usuarioResult.rows[0].nivel_acceso;

    // Realiza una consulta para obtener el nombre del nivel de acceso
    const getNivelAccesoByIdQuery = 'SELECT nombre FROM nivel_acceso WHERE id = $1;';
    const nivelAccesoResult = await pool.query(getNivelAccesoByIdQuery, [nivelAccesoId]);

    // Si no hay datos para el nivel de acceso, devolver un mensaje adecuado
    if (nivelAccesoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Nivel de acceso no encontrado para el ID proporcionado.' });
    }

    const nombreNivelAcceso = nivelAccesoResult.rows[0].nombre;

    // Devuelve el usuario encontrado junto con el nombre del nivel de acceso como parte de la respuesta
    res.status(200).json({
      message: 'Usuario obtenido con éxito',
      usuario: {
        ...usuarioResult.rows[0],
        nombre_nivel_acceso: nombreNivelAcceso,
      },
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


app.get('/api/horarios_medicos', async (req, res) => {
  console.log('Solicitud de todos los horarios médicos recibida');

  try {
    // Realiza una consulta para obtener todos los horarios médicos
    const getAllHorariosMedicosQuery = 'SELECT * FROM horarios_medicos;';
    const horariosMedicosResult = await pool.query(getAllHorariosMedicosQuery);

    // Si no hay datos en los horarios médicos, devolver un mensaje adecuado
    if (horariosMedicosResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron horarios médicos.' });
    }

    // Obtener información del médico asociada a los IDs de médico
    const idsMedico = horariosMedicosResult.rows.map(row => row.medico_id);
    const getMedicoInfoQuery = `SELECT id_usuarios as medico_id, nombre, num_colegiado FROM usuarios WHERE id_usuarios IN (${idsMedico.join(',')});`;
    const medicoInfoResult = await pool.query(getMedicoInfoQuery);

    // Crear un mapa para buscar fácilmente la información del médico por medico_id
    const medicoInfoMap = {};
    medicoInfoResult.rows.forEach(row => {
      medicoInfoMap[row.medico_id] = {
        nombre: row.nombre,
        num_colegiado: row.num_colegiado,
      };
    });

    // Agregar información del médico a los horarios médicos
    const horariosConInfoMedico = horariosMedicosResult.rows.map(row => {
      const medicoInfo = medicoInfoMap[row.medico_id] || {};
      return {
        id: row.id,
        medico_id: row.medico_id,
        nombre_medico: medicoInfo.nombre || '',
        num_colegiado_medico: medicoInfo.num_colegiado || '',
        dia_semana: row.dia_semana,
        hora_inicio: row.hora_inicio,
        hora_fin: row.hora_fin,
      };
    });

    // Devuelve todos los horarios médicos con información del médico como parte de la respuesta
    res.status(200).json({ message: 'Horarios médicos obtenidos con éxito', horarios_medicos: horariosConInfoMedico });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

app.get('/api/horarios_medicos/:id', async (req, res) => {
  const horarioMedicoId = req.params.id;

  console.log(`Solicitud del horario médico con ID ${horarioMedicoId}`);

  try {
    // Realiza una consulta para obtener el horario médico por ID
    const getHorarioMedicoByIdQuery = `SELECT * FROM horarios_medicos WHERE id = ${horarioMedicoId};`;
    const horarioMedicoResult = await pool.query(getHorarioMedicoByIdQuery);

    // Si no se encuentra el horario médico, devuelve un mensaje adecuado
    if (horarioMedicoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Horario médico no encontrado.' });
    }

    // Obtener información del médico asociada al ID de médico
    const medicoId = horarioMedicoResult.rows[0].medico_id;
    const getMedicoInfoQuery = `SELECT id_usuarios as medico_id, nombre, num_colegiado FROM usuarios WHERE id_usuarios = ${medicoId};`;
    const medicoInfoResult = await pool.query(getMedicoInfoQuery);

    // Si no se encuentra la información del médico, devuelve un mensaje adecuado
    if (medicoInfoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Información del médico no encontrada.' });
    }

    // Crear un objeto con la información del horario médico y del médico
    const horarioConInfoMedico = {
      id: horarioMedicoResult.rows[0].id,
      medico_id: medicoId,
      nombre_medico: medicoInfoResult.rows[0].nombre || '',
      num_colegiado_medico: medicoInfoResult.rows[0].num_colegiado || '',
      dia_semana: horarioMedicoResult.rows[0].dia_semana,
      hora_inicio: horarioMedicoResult.rows[0].hora_inicio,
      hora_fin: horarioMedicoResult.rows[0].hora_fin,
    };

    // Devuelve el horario médico con información del médico como parte de la respuesta
    res.status(200).json({ message: 'Horario médico obtenido con éxito', horario_medico: horarioConInfoMedico });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

app.get('/api/horarios_medicos/medico/:num_colegiado', async (req, res) => {
  const numColegiadoMedico = req.params.num_colegiado;

  console.log(`Solicitud de horarios médicos para el médico con número de colegiado ${numColegiadoMedico}`);

  try {
    // Realiza una consulta para obtener el ID del médico por número de colegiado
    const getMedicoIdByNumColegiadoQuery = `SELECT id_usuarios FROM usuarios WHERE num_colegiado = '${numColegiadoMedico}';`;
    const medicoIdResult = await pool.query(getMedicoIdByNumColegiadoQuery);

    // Si no se encuentra el médico, devuelve un mensaje adecuado
    if (medicoIdResult.rows.length === 0) {
      return res.status(404).json({ message: 'Médico no encontrado.' });
    }

    const medicoId = medicoIdResult.rows[0].id_usuarios;

    // Realiza una consulta para obtener los horarios médicos para el médico con el ID obtenido
    const getHorariosMedicosByMedicoIdQuery = `SELECT * FROM horarios_medicos WHERE medico_id = ${medicoId};`;
    const horariosMedicosResult = await pool.query(getHorariosMedicosByMedicoIdQuery);

    // Si no se encuentran horarios médicos para el médico, devuelve un mensaje adecuado
    if (horariosMedicosResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron horarios médicos para este médico.' });
    }

    // Obtener información del médico asociada al ID de médico
    const getMedicoInfoQuery = `SELECT nombre FROM usuarios WHERE id_usuarios = ${medicoId};`;
    const medicoInfoResult = await pool.query(getMedicoInfoQuery);

    // Si no se encuentra la información del médico, devuelve un mensaje adecuado
    if (medicoInfoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Información del médico no encontrada.' });
    }

    const nombreMedico = medicoInfoResult.rows[0].nombre;

    // Crear un arreglo con la información de los horarios médicos y del médico
    const horariosConInfoMedico = horariosMedicosResult.rows.map(row => ({
      id: row.id,
      medico_id: medicoId,
      nombre_medico: nombreMedico,
      num_colegiado_medico: numColegiadoMedico,
      dia_semana: row.dia_semana,
      hora_inicio: row.hora_inicio,
      hora_fin: row.hora_fin,
    }));

    // Devuelve los horarios médicos con información del médico como parte de la respuesta
    res.status(200).json({ message: 'Horarios médicos obtenidos con éxito', horarios_medicos: horariosConInfoMedico });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

app.get('/api/laboratorios', async (req, res) => {
  console.log('Solicitud de todos los laboratorios recibida');

  try {
    // Realiza una consulta para obtener todos los laboratorios con información del paciente
    const getAllLaboratoriosQuery = `
      SELECT lab.id,lab.paciente_id,cli.nombre AS nombre_paciente,cli.primer_apellido AS primer_apellido_paciente,cli.segundo_apellido AS segundo_apellido_paciente,lab.tipo_prueba,lab.resultado,
        lab.fecha_realizacionFROM laboratorio labJOIN cliente cli ON lab.paciente_id = cli.dni;`;

    const laboratoriosResult = await pool.query(getAllLaboratoriosQuery);

    // Si no hay datos en los laboratorios, devolver un mensaje adecuado
    if (laboratoriosResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron resultados de laboratorios.' });
    }

    // Devolver todos los resultados de laboratorios con información del paciente como parte de la respuesta
    res.status(200).json({ message: 'Resultados de laboratorios obtenidos con éxito', laboratorios: laboratoriosResult.rows });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});



app.get('/api/laboratorios/:id', async (req, res) => {
  const laboratorioId = req.params.id;

  console.log(`Solicitud del resultado de laboratorio con ID ${laboratorioId}`);

  try {
    // Realiza una consulta para obtener el resultado de laboratorio por ID
    const getLaboratorioByIdQuery = `SELECT * FROM laboratorio WHERE id = ${laboratorioId};`;
    const laboratorioResult = await pool.query(getLaboratorioByIdQuery);

    // Si no se encuentra el resultado de laboratorio, devuelve un mensaje adecuado
    if (laboratorioResult.rows.length === 0) {
      return res.status(404).json({ message: 'Resultado de laboratorio no encontrado.' });
    }

    // Obtener información del paciente asociada al ID de paciente
    const pacienteId = laboratorioResult.rows[0].paciente_id;
    const getPacienteInfoQuery = `SELECT dni as paciente_id, nombre, primer_apellido, segundo_apellido FROM cliente WHERE dni = '${pacienteId}';`;
    const pacienteInfoResult = await pool.query(getPacienteInfoQuery);

    // Si no se encuentra la información del paciente, devuelve un mensaje adecuado
    if (pacienteInfoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Información del paciente no encontrada.' });
    }

    const pacienteInfo = pacienteInfoResult.rows[0];

    // Crear un objeto con la información del resultado de laboratorio y del paciente
    const laboratorioConInfoPaciente = {
      id: laboratorioResult.rows[0].id,
      paciente_id: pacienteId,
      nombre_paciente: pacienteInfo.nombre || '',
      apellidos_paciente: `${pacienteInfo.primer_apellido || ''} ${pacienteInfo.segundo_apellido || ''}`,
      tipo_prueba: laboratorioResult.rows[0].tipo_prueba,
      resultado: laboratorioResult.rows[0].resultado,
      fecha_realizacion: laboratorioResult.rows[0].fecha_realizacion,
    };

    // Devuelve el resultado de laboratorio con información del paciente como parte de la respuesta
    res.status(200).json({ message: 'Resultado de laboratorio obtenido con éxito', laboratorio: laboratorioConInfoPaciente });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

app.get('/api/laboratorios/cliente/:id', async (req, res) => {
  const clienteId = req.params.id;

  console.log(`Solicitud de resultados de laboratorio para el cliente con ID ${clienteId}`);

  try {
    // Realiza una consulta para obtener los resultados de laboratorio para el cliente con el ID proporcionado
    const getLaboratoriosByClienteIdQuery = `SELECT * FROM laboratorio WHERE paciente_id = '${clienteId}';`;
    const laboratoriosResult = await pool.query(getLaboratoriosByClienteIdQuery);

    // Si no se encuentran resultados de laboratorio para el cliente, devuelve un mensaje adecuado
    if (laboratoriosResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron resultados de laboratorio para este cliente.' });
    }

    // Obtener información del cliente asociada al ID de cliente
    const getClienteInfoQuery = `SELECT dni as paciente_id, nombre, primer_apellido, segundo_apellido FROM cliente WHERE dni = '${clienteId}';`;
    const clienteInfoResult = await pool.query(getClienteInfoQuery);

    // Si no se encuentra la información del cliente, devuelve un mensaje adecuado
    if (clienteInfoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Información del cliente no encontrada.' });
    }

    const clienteInfo = clienteInfoResult.rows[0];

    // Crear un arreglo con la información de los resultados de laboratorio y del cliente
    const laboratoriosConInfoCliente = laboratoriosResult.rows.map(row => ({
      id: row.id,
      paciente_id: clienteId,
      nombre_paciente: clienteInfo.nombre || '',
      apellidos_paciente: `${clienteInfo.primer_apellido || ''} ${clienteInfo.segundo_apellido || ''}`,
      tipo_prueba: row.tipo_prueba,
      resultado: row.resultado,
      fecha_realizacion: row.fecha_realizacion,
    }));

    // Devuelve los resultados de laboratorio con información del cliente como parte de la respuesta
    res.status(200).json({ message: 'Resultados de laboratorio obtenidos con éxito', laboratorios: laboratoriosConInfoCliente });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

app.get('/api/fechas_vacaciones', async (req, res) => {
  console.log('Solicitud de todas las fechas de vacaciones recibida');

  try {
    // Realiza una consulta para obtener todas las fechas de vacaciones
    const getAllFechasVacacionesQuery = 'SELECT * FROM fechas_vacaciones;';
    const fechasVacacionesResult = await pool.query(getAllFechasVacacionesQuery);

    // Si no hay datos en las fechas de vacaciones, devolver un mensaje adecuado
    if (fechasVacacionesResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron fechas de vacaciones.' });
    }

    // Obtener información del médico asociada a los IDs de médico
    const idsMedico = fechasVacacionesResult.rows.map(row => row.medico_id);
    const getMedicoInfoQuery = `SELECT id_usuarios as medico_id, nombre, num_colegiado FROM usuarios WHERE id_usuarios IN (${idsMedico.join(',')});`;
    const medicoInfoResult = await pool.query(getMedicoInfoQuery);

    // Crear un mapa para buscar fácilmente la información del médico por medico_id
    const medicoInfoMap = {};
    medicoInfoResult.rows.forEach(row => {
      medicoInfoMap[row.medico_id] = {
        nombre: row.nombre,
        num_colegiado: row.num_colegiado,
      };
    });

    // Agregar información del médico a los resultados de fechas de vacaciones
    const fechasVacacionesConInfoMedico = fechasVacacionesResult.rows.map(row => {
      const medicoInfo = medicoInfoMap[row.medico_id] || {};
      return {
        id: row.id,
        medico_id: row.medico_id,
        nombre_medico: medicoInfo.nombre || '',
        num_colegiado_medico: medicoInfo.num_colegiado || '',
        fecha_inicio_vacaciones: row.fecha_inicio_vacaciones,
        fecha_fin_vacaciones: row.fecha_fin_vacaciones,
      };
    });

    // Devuelve todas las fechas de vacaciones con información del médico como parte de la respuesta
    res.status(200).json({ message: 'Fechas de vacaciones obtenidas con éxito', fechasVacaciones: fechasVacacionesConInfoMedico });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

app.get('/api/fechas_vacaciones/:id', async (req, res) => {
  const fechaVacacionesId = req.params.id;

  console.log(`Solicitud de información para la fecha de vacaciones con ID ${fechaVacacionesId}`);

  try {
    // Realiza una consulta para obtener la información de la fecha de vacaciones específica
    const getFechaVacacionesQuery = `
      SELECT 
        id,
        medico_id,
        fecha_inicio_vacaciones,
        fecha_fin_vacaciones
      FROM fechas_vacaciones
      WHERE id = $1;
    `;

    const fechaVacacionesResult = await pool.query(getFechaVacacionesQuery, [fechaVacacionesId]);

    // Si no hay datos para la fecha de vacaciones específica, devolver un mensaje adecuado
    if (fechaVacacionesResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontró información para la fecha de vacaciones especificada.' });
    }

    // Obtener información del médico asociada a la fecha de vacaciones
    const medicoId = fechaVacacionesResult.rows[0].medico_id;
    const getMedicoInfoQuery = `SELECT id_usuarios as medico_id, nombre, num_colegiado FROM usuarios WHERE id_usuarios = $1;`;
    const medicoInfoResult = await pool.query(getMedicoInfoQuery, [medicoId]);

    // Si no hay datos para el médico asociado, devolver un mensaje adecuado
    if (medicoInfoResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontró información del médico asociado a la fecha de vacaciones.' });
    }

    const medicoInfo = medicoInfoResult.rows[0];

    // Devolver la información de la fecha de vacaciones y del médico como parte de la respuesta
    res.status(200).json({
      message: 'Información de la fecha de vacaciones obtenida con éxito',
      fechaVacaciones: {
        id: fechaVacacionesResult.rows[0].id,
        medico_id: medicoInfo.medico_id,
        nombre_medico: medicoInfo.nombre || '',
        num_colegiado_medico: medicoInfo.num_colegiado || '',
        fecha_inicio_vacaciones: fechaVacacionesResult.rows[0].fecha_inicio_vacaciones,
        fecha_fin_vacaciones: fechaVacacionesResult.rows[0].fecha_fin_vacaciones,
      },
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

app.get('/api/fechas_vacaciones/:num_colegiado', async (req, res) => {
  const numColegiado = req.params.num_colegiado;

  console.log(`Solicitud de fechas de vacaciones para el médico con número de colegiado ${numColegiado}`);

  try {
    // Obtener información del médico por número de colegiado
    const getMedicoInfoQuery = 'SELECT id_usuarios as medico_id, nombre, num_colegiado FROM usuarios WHERE num_colegiado = $1;';
    const medicoInfoResult = await pool.query(getMedicoInfoQuery, [numColegiado]);

    // Si no se encuentra la información del médico, devuelve un mensaje adecuado
    if (medicoInfoResult.rows.length === 0) {
      return res.status(404).json({ message: 'Información del médico no encontrada.' });
    }

    const medicoInfo = medicoInfoResult.rows[0];
    const medicoId = medicoInfo.medico_id;

    // Obtener fechas de vacaciones del médico por ID
    const getFechasVacacionesQuery = 'SELECT * FROM fechas_vacaciones WHERE medico_id = $1;';
    const fechasVacacionesResult = await pool.query(getFechasVacacionesQuery, [medicoId]);

    // Crear un arreglo con la información de las fechas de vacaciones
    const fechasVacaciones = fechasVacacionesResult.rows.map(row => ({
      id: row.id,
      fecha_inicio_vacaciones: row.fecha_inicio_vacaciones,
      fecha_fin_vacaciones: row.fecha_fin_vacaciones,
    }));

    // Devolver la información del médico y sus fechas de vacaciones como parte de la respuesta
    res.status(200).json({
      message: 'Información del médico y sus fechas de vacaciones obtenida con éxito',
      medico: {
        medico_id: medicoInfo.medico_id,
        nombre_medico: medicoInfo.nombre || '',
        num_colegiado_medico: medicoInfo.num_colegiado || '',
      },
      fechasVacaciones: fechasVacaciones,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Ruta para obtener todos los medicamentos con información adicional
app.get('/api/medicamentos', async (req, res) => {
  console.log('Solicitud de todos los medicamentos recibida');

  try {
    // Realiza una consulta para obtener todos los medicamentos
    const getAllMedicamentosQuery = 'SELECT id, nombre, id_fabricante, id_tipo, contraindicaciones, nrregistro FROM medicamento;';
    const medicamentosResult = await pool.query(getAllMedicamentosQuery);

    // Si no hay datos en la tabla de medicamentos, devolver un mensaje adecuado
    if (medicamentosResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron medicamentos.' });
    }

    // Obtener nombres de fabricante y tipo de medicamento para cada medicamento
    const idsFabricante = medicamentosResult.rows.map(row => row.id_fabricante);
    const idsTipoMedicamento = medicamentosResult.rows.map(row => row.id_tipo);

    const getFabricanteQuery = `SELECT id_fabricante, nombre FROM fabricante WHERE id_fabricante IN (${idsFabricante.join(',')});`;
    const getTipoMedicamentoQuery = `SELECT id, tipo FROM tipomedicamento WHERE id IN (${idsTipoMedicamento.join(',')});`;

    const fabricanteResult = await pool.query(getFabricanteQuery);
    const tipoMedicamentoResult = await pool.query(getTipoMedicamentoQuery);

    // Crear mapas para buscar fácilmente la información por ID
    const fabricanteMap = {};
    const tipoMedicamentoMap = {};

    fabricanteResult.rows.forEach(row => {
      fabricanteMap[row.id_fabricante] = row.nombre;
    });

    tipoMedicamentoResult.rows.forEach(row => {
      tipoMedicamentoMap[row.id] = row.tipo;
    });

    // Procesa los datos de medicamentos con información adicional
    const medicamentos = medicamentosResult.rows.map(row => ({
      id: row.id,
      nombre: row.nombre,
      id_fabricante: row.id_fabricante,
      nombre_fabricante: fabricanteMap[row.id_fabricante] || '', // Obtiene el nombre del fabricante del mapa
      id_tipo: row.id_tipo,
      tipo_medicamento: tipoMedicamentoMap[row.id_tipo] || 'Tipo no especificado', // Agrega un valor predeterminado si no se encuentra el tipo
      contraindicaciones: row.contraindicaciones,
      nrregistro: row.nrregistro,
    }));

    // Devuelve todos los medicamentos como parte de la respuesta
    res.status(200).json({ message: 'Medicamentos obtenidos con éxito', medicamentos });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Ruta para obtener todas las citas con información adicional
app.get('/api/citas', async (req, res) => {
  console.log('Solicitud de todas las citas recibida');

  try {
    // Realiza una consulta para obtener todas las citas
    const getAllCitasQuery = 'SELECT id, paciente_id, medico_id, fecha_cita, estado FROM citas;';
    const citasResult = await pool.query(getAllCitasQuery);

    // Si no hay datos en la tabla de citas, devolver un mensaje adecuado
    if (citasResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron citas.' });
    }

    // Obtener nombres de paciente y médico para cada cita
    const idsPaciente = citasResult.rows.map(row => `'${row.paciente_id}'`);
    const idsMedico = citasResult.rows.map(row => row.medico_id);

    const getPacienteInfoQuery = `SELECT dni as paciente_id, nombre, primer_apellido, segundo_apellido FROM cliente WHERE dni IN (${idsPaciente.join(',')});`;
    const getMedicoInfoQuery = `SELECT id_usuarios as medico_id, nombre, num_colegiado FROM usuarios WHERE id_usuarios IN (${idsMedico.join(',')});`;

    const pacienteInfoResult = await pool.query(getPacienteInfoQuery);
    const medicoInfoResult = await pool.query(getMedicoInfoQuery);

    // Crear mapas para buscar fácilmente la información por ID
    const pacienteInfoMap = {};
    const medicoInfoMap = {};

    pacienteInfoResult.rows.forEach(row => {
      pacienteInfoMap[row.paciente_id] = {
        nombre: row.nombre,
        primer_apellido: row.primer_apellido,
        segundo_apellido: row.segundo_apellido,
      };
    });

    medicoInfoResult.rows.forEach(row => {
      medicoInfoMap[row.medico_id] = {
        nombre: row.nombre,
        num_colegiado: row.num_colegiado,
      };
    });

    // Procesa los datos de citas con información adicional
    const citas = citasResult.rows.map(row => ({
      id: row.id,
      paciente_id: row.paciente_id,
      nombre_paciente: pacienteInfoMap[row.paciente_id]?.nombre || '',
      apellidos_paciente: `${pacienteInfoMap[row.paciente_id]?.primer_apellido || ''} ${pacienteInfoMap[row.paciente_id]?.segundo_apellido || ''}`,
      medico_id: row.medico_id,
      nombre_medico: medicoInfoMap[row.medico_id]?.nombre || '',
      num_colegiado_medico: medicoInfoMap[row.medico_id]?.num_colegiado || '',
      fecha_cita: row.fecha_cita,
      estado: row.estado,
    }));

    // Devuelve todas las citas como parte de la respuesta
    res.status(200).json({ message: 'Citas obtenidas con éxito', citas });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

/// Ruta para obtener todas las citas con información adicional para un paciente específico
app.get('/api/citas/:idPaciente', async (req, res) => {
  const idPaciente = req.params.idPaciente;

  console.log(`Solicitud de citas para el paciente con ID ${idPaciente} recibida`);

  try {
    // Realiza una consulta para obtener todas las citas para el paciente específico
    const getCitasPorPacienteQuery = 'SELECT id, paciente_id, medico_id, fecha_cita, estado FROM citas WHERE paciente_id = $1;';
    const citasResult = await pool.query(getCitasPorPacienteQuery, [idPaciente]);

    // Si no hay datos en la tabla de citas para el paciente, devolver un mensaje adecuado
    if (citasResult.rows.length === 0) {
      return res.status(404).json({ message: `No se encontraron citas para el paciente con ID ${idPaciente}.` });
    }

    // Obtener información de paciente y médico
    const idsMedico = citasResult.rows.map(row => row.medico_id);

    const getMedicoInfoQuery = `SELECT id_usuarios as medico_id, nombre, num_colegiado FROM usuarios WHERE id_usuarios IN (${idsMedico.join(',')});`;

    const medicoInfoResult = await pool.query(getMedicoInfoQuery);

    // Crear un mapa para buscar fácilmente la información por ID
    const medicoInfoMap = {};

    medicoInfoResult.rows.forEach(row => {
      medicoInfoMap[row.medico_id] = {
        nombre: row.nombre,
        num_colegiado: row.num_colegiado,
      };
    });

    // Obtener información de paciente
    const idsPaciente = citasResult.rows.map(row => `'${row.paciente_id}'`);
    const getPacienteInfoQuery = `SELECT dni as paciente_id, nombre, primer_apellido, segundo_apellido FROM cliente WHERE dni IN (${idsPaciente.join(',')});`;
    const pacienteInfoResult = await pool.query(getPacienteInfoQuery);

    // Crear un mapa para buscar fácilmente la información del paciente por ID
    const pacienteInfoMap = {};
    pacienteInfoResult.rows.forEach(row => {
      pacienteInfoMap[row.paciente_id] = {
        nombre: row.nombre,
        primer_apellido: row.primer_apellido,
        segundo_apellido: row.segundo_apellido,
      };
    });

    // Procesar los datos de citas con información adicional
    const citas = citasResult.rows.map(row => ({
      id: row.id,
      paciente_id: row.paciente_id,
      nombre_paciente: pacienteInfoMap[row.paciente_id]?.nombre || '',
      apellidos_paciente: `${pacienteInfoMap[row.paciente_id]?.primer_apellido || ''} ${pacienteInfoMap[row.paciente_id]?.segundo_apellido || ''}`,
      medico_id: row.medico_id,
      nombre_medico: medicoInfoMap[row.medico_id]?.nombre || '',
      num_colegiado_medico: medicoInfoMap[row.medico_id]?.num_colegiado || '',
      fecha_cita: row.fecha_cita,
      estado: row.estado,
    }));

    // Devolver todas las citas para el paciente como parte de la respuesta
    res.status(200).json({ message: `Citas obtenidas con éxito para el paciente con ID ${idPaciente}`, citas });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Ruta para obtener todas las citas con información adicional para un médico específico
app.get('/api/citas/medico/:numColegiado', async (req, res) => {
  const numColegiado = req.params.numColegiado;

  console.log(`Solicitud de citas para el médico con número de colegiado ${numColegiado} recibida`);

  try {
    // Realiza una consulta para obtener todas las citas para el médico específico
    const getCitasPorMedicoQuery = 'SELECT id, paciente_id, medico_id, fecha_cita, estado FROM citas WHERE medico_id IN (SELECT id_usuarios FROM usuarios WHERE num_colegiado = $1);';
    const citasResult = await pool.query(getCitasPorMedicoQuery, [numColegiado]);

    // Si no hay datos en la tabla de citas para el médico, devolver un mensaje adecuado
    if (citasResult.rows.length === 0) {
      return res.status(404).json({ message: `No se encontraron citas para el médico con número de colegiado ${numColegiado}.` });
    }

    // Obtener información de paciente y médico
    const idsPaciente = citasResult.rows.map(row => `'${row.paciente_id}'`);
    const getPacienteInfoQuery = `SELECT dni as paciente_id, nombre, primer_apellido, segundo_apellido FROM cliente WHERE dni IN (${idsPaciente.join(',')});`;
    const pacienteInfoResult = await pool.query(getPacienteInfoQuery);

    // Crear un mapa para buscar fácilmente la información del paciente por ID
    const pacienteInfoMap = {};
    pacienteInfoResult.rows.forEach(row => {
      pacienteInfoMap[row.paciente_id] = {
        nombre: row.nombre,
        primer_apellido: row.primer_apellido,
        segundo_apellido: row.segundo_apellido,
      };
    });

    // Procesar los datos de citas con información adicional
    const citas = citasResult.rows.map(row => ({
      id: row.id,
      paciente_id: row.paciente_id,
      nombre_paciente: pacienteInfoMap[row.paciente_id]?.nombre || '',
      apellidos_paciente: `${pacienteInfoMap[row.paciente_id]?.primer_apellido || ''} ${pacienteInfoMap[row.paciente_id]?.segundo_apellido || ''}`,
      medico_id: row.medico_id,
      fecha_cita: row.fecha_cita,
      estado: row.estado,
    }));

    // Devolver todas las citas para el médico como parte de la respuesta
    res.status(200).json({ message: `Citas obtenidas con éxito para el médico con número de colegiado ${numColegiado}`, citas });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

app.get('/api/cliente_medicamentos', async (req, res) => {
  console.log('Solicitud de todos los registros de cliente_medicamentos recibida');

  try {
    // Realizar consulta para obtener todos los registros de cliente_medicamentos
    const getAllClienteMedicamentosQuery = 'SELECT * FROM cliente_medicamentos;';
    const clienteMedicamentosResult = await pool.query(getAllClienteMedicamentosQuery);

    // Si no hay datos en cliente_medicamentos, devolver un mensaje adecuado
    if (clienteMedicamentosResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron registros en cliente_medicamentos.' });
    }

    // Obtener información del cliente
    const clienteIds = clienteMedicamentosResult.rows.map(row => `'${row.cliente_id}'`);
    const clienteInfoQuery = `SELECT dni as cliente_id, nombre, primer_apellido, segundo_apellido FROM cliente WHERE dni IN (${clienteIds.join(',')});`;
    const clienteInfoResult = await pool.query(clienteInfoQuery);

    // Crear un mapa para buscar fácilmente la información del cliente por cliente_id
    const clienteInfoMap = {};
    clienteInfoResult.rows.forEach(row => {
      clienteInfoMap[row.cliente_id] = {
        nombre: row.nombre,
        primer_apellido: row.primer_apellido,
        segundo_apellido: row.segundo_apellido,
      };
    });

    // Obtener información del usuario
    const usuarioIds = clienteMedicamentosResult.rows.map(row => row.usuario_id);
    const usuarioInfoQuery = `SELECT id_usuarios as usuario_id, nombre, num_colegiado FROM usuarios WHERE id_usuarios IN (${usuarioIds.join(',')});`;
    const usuarioInfoResult = await pool.query(usuarioInfoQuery);

    // Crear un mapa para buscar fácilmente la información del usuario por usuario_id
    const usuarioInfoMap = {};
    usuarioInfoResult.rows.forEach(row => {
      usuarioInfoMap[row.usuario_id] = {
        nombre_usuario: row.nombre,
        num_colegiado: row.num_colegiado,
      };
    });

    // Agregar información del cliente y usuario a los registros de cliente_medicamentos
    const registrosClienteMedicamentos = clienteMedicamentosResult.rows.map(row => {
      const clienteInfo = clienteInfoMap[row.cliente_id] || {};
      const usuarioInfo = usuarioInfoMap[row.usuario_id] || {};
      return {
        id: row.id,
        cliente_id: row.cliente_id,
        nombre_cliente: `${clienteInfo.nombre || ''} ${clienteInfo.primer_apellido || ''} ${clienteInfo.segundo_apellido || ''}`,
        usuario_id: row.usuario_id,
        nombre_usuario: usuarioInfo.nombre_usuario || '',
        num_colegiado_usuario: usuarioInfo.num_colegiado || '',
        medicamento_id: row.medicamento_id,
        dosis: row.dosis,
        fecha_ini: row.fecha_ini,
        fecha_fin: row.fecha_fin,
      };
    });

    // Devuelve todos los registros de cliente_medicamentos con información del cliente y usuario como parte de la respuesta
    res.status(200).json({ message: 'Registros de cliente_medicamentos obtenidos con éxito', cliente_medicamentos: registrosClienteMedicamentos });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

app.get('/api/cliente_medicamentos/:id', async (req, res) => {
  const clienteMedicamentoId = req.params.id;

  console.log(`Solicitud del registro de cliente_medicamentos con ID ${clienteMedicamentoId}`);

  try {
    // Realizar consulta para obtener el registro de cliente_medicamentos por ID
    const getClienteMedicamentoByIdQuery = `SELECT * FROM cliente_medicamentos WHERE id = ${clienteMedicamentoId};`;
    const clienteMedicamentoResult = await pool.query(getClienteMedicamentoByIdQuery);

    // Si no hay datos para el ID proporcionado, devolver un mensaje adecuado
    if (clienteMedicamentoResult.rows.length === 0) {
      return res.status(404).json({ message: `No se encontró el registro en cliente_medicamentos con ID ${clienteMedicamentoId}.` });
    }

    // Obtener información del cliente
    const clienteInfoQuery = `SELECT dni as cliente_id, nombre, primer_apellido, segundo_apellido FROM cliente WHERE dni = '${clienteMedicamentoResult.rows[0].cliente_id}';`;
    const clienteInfoResult = await pool.query(clienteInfoQuery);

    // Obtener información del usuario
    const usuarioInfoQuery = `SELECT id_usuarios as usuario_id, nombre, num_colegiado FROM usuarios WHERE id_usuarios = ${clienteMedicamentoResult.rows[0].usuario_id};`;
    const usuarioInfoResult = await pool.query(usuarioInfoQuery);

    // Crear un objeto con información del cliente, usuario y el registro de cliente_medicamentos
    const clienteMedicamentoInfo = {
      id: clienteMedicamentoResult.rows[0].id,
      cliente_id: clienteMedicamentoResult.rows[0].cliente_id,
      nombre_cliente: `${clienteInfoResult.rows[0].nombre || ''} ${clienteInfoResult.rows[0].primer_apellido || ''} ${clienteInfoResult.rows[0].segundo_apellido || ''}`,
      usuario_id: clienteMedicamentoResult.rows[0].usuario_id,
      nombre_usuario: usuarioInfoResult.rows[0].nombre || '',
      num_colegiado_usuario: usuarioInfoResult.rows[0].num_colegiado || '',
      medicamento_id: clienteMedicamentoResult.rows[0].medicamento_id,
      dosis: clienteMedicamentoResult.rows[0].dosis,
      fecha_ini: clienteMedicamentoResult.rows[0].fecha_ini,
      fecha_fin: clienteMedicamentoResult.rows[0].fecha_fin,
    };

    // Devuelve la información del registro de cliente_medicamentos con información del cliente y usuario como parte de la respuesta
    res.status(200).json({ message: `Registro de cliente_medicamentos con ID ${clienteMedicamentoId} obtenido con éxito`, cliente_medicamento: clienteMedicamentoInfo });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

app.get('/api/cliente_medicamentos/:num_colegiado', async (req, res) => {
  console.log('Solicitud de registros de cliente_medicamentos por número de colegiado recibida');

  try {
    // Obtener el número de colegiado del parámetro de la URL
    const numColegiadoFiltro = req.params.num_colegiado;

    // Realizar consulta para obtener los registros de cliente_medicamentos filtrados por número de colegiado
    const getAllClienteMedicamentosQuery = `
      SELECT * FROM cliente_medicamentos
      WHERE usuario_id IN (SELECT id_usuarios FROM usuarios WHERE num_colegiado = '${numColegiadoFiltro}');
    `;

    const clienteMedicamentosResult = await pool.query(getAllClienteMedicamentosQuery);

    // Si no hay datos en cliente_medicamentos, devolver un mensaje adecuado
    if (clienteMedicamentosResult.rows.length === 0) {
      return res.status(404).json({ message: `No se encontraron registros en cliente_medicamentos para el número de colegiado ${numColegiadoFiltro}.` });
    }

    // Obtener información del cliente y del usuario...
    
    // Agregar descripciones y datos del cliente y usuario a los registros de cliente_medicamentos
    const registrosConDescripciones = clienteMedicamentosResult.rows.map(row => {
      const clienteInfo = clienteInfoMap[row.cliente_id] || {};
      const usuarioInfo = usuarioInfoMap[row.usuario_id] || {};
      return {
        id: row.id,
        cliente_id: row.cliente_id,
        nombre_cliente: clienteInfo.nombre || '',
        apellidos_cliente: `${clienteInfo.primer_apellido || ''} ${clienteInfo.segundo_apellido || ''}`,
        nombre_usuario: usuarioInfo.nombre_usuario || '',
        num_colegiado_usuario: usuarioInfo.num_colegiado || '',
        fecha_registro: row.fecha_registro,
        // Otros campos específicos de cliente_medicamentos...
      };
    });

    // Devuelve los registros de cliente_medicamentos con descripciones como parte de la respuesta
    res.status(200).json({ message: 'Registros de cliente_medicamentos obtenidos con éxito', cliente_medicamentos: registrosConDescripciones });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

app.get('/api/cliente_medicamentos/:num_colegiado', async (req, res) => {
  console.log('Solicitud de registros de cliente_medicamentos por número de colegiado recibida');

  try {
    const num_colegiados = req.params.num_colegiado.split(','); // Puedes proporcionar varios números separados por comas

    // Realizar consulta para obtener registros de cliente_medicamentos por número de colegiado
    const getClienteMedicamentosByNumColegiadoQuery = `
      SELECT * FROM cliente_medicamentos
      WHERE usuario_id IN (SELECT id_usuarios FROM usuarios WHERE num_colegiado IN (${num_colegiados.map((_, i) => `$${i + 1}`).join(',')}));
    `;

    const clienteMedicamentosResult = await pool.query(getClienteMedicamentosByNumColegiadoQuery, num_colegiados);

    // Verificar si se encontraron registros
    if (clienteMedicamentosResult.rows.length === 0) {
      return res.status(404).json({ message: 'No se encontraron registros en cliente_medicamentos para los números de colegiado proporcionados.' });
    }

    // Obtener información del cliente
    const clienteIds = clienteMedicamentosResult.rows.map(row => `'${row.cliente_id}'`);
    const clienteInfoQuery = `SELECT dni as cliente_id, nombre, primer_apellido, segundo_apellido FROM cliente WHERE dni IN (${clienteIds.join(',')});`;
    const clienteInfoResult = await pool.query(clienteInfoQuery);

    // Crear un mapa para buscar fácilmente la información del cliente por cliente_id
    const clienteInfoMap = {};
    clienteInfoResult.rows.forEach(row => {
      clienteInfoMap[row.cliente_id] = {
        nombre: row.nombre,
        primer_apellido: row.primer_apellido,
        segundo_apellido: row.segundo_apellido,
      };
    });

    // Obtener información del usuario
    const usuarioIds = clienteMedicamentosResult.rows.map(row => row.usuario_id);
    // Corregir el nombre de la tabla a 'usuarios' en lugar de 'usuarios'
    const usuarioInfoQuery = `SELECT id_usuarios as usuario_id, nombre, num_colegiado FROM usuarios WHERE id_usuarios IN (${usuarioIds.join(',')});`;
    const usuarioInfoResult = await pool.query(usuarioInfoQuery);

    // Crear un mapa para buscar fácilmente la información del usuario por usuario_id
    const usuarioInfoMap = {};
    usuarioInfoResult.rows.forEach(row => {
      usuarioInfoMap[row.usuario_id] = {
        nombre_usuario: row.nombre,
        num_colegiado: row.num_colegiado,
      };
    });

    // Procesar los datos según sea necesario
    const datosProcesados = clienteMedicamentosResult.rows.map(row => {
      const clienteInfo = clienteInfoMap[row.cliente_id] || {};
      const usuarioInfo = usuarioInfoMap[row.usuario_id] || {};
      return {
        id: row.id,
        cliente_id: row.cliente_id,
        nombre_cliente: clienteInfo.nombre || '',
        apellidos_cliente: `${clienteInfo.primer_apellido || ''} ${clienteInfo.segundo_apellido || ''}`,
        nombre_usuario: usuarioInfo.nombre_usuario || '',
        num_colegiado_usuario: usuarioInfo.num_colegiado || '',
      };
    });

    // Devolver una respuesta exitosa con la información requerida
    res.status(200).json({ message: 'Registros de cliente_medicamentos obtenidos con éxito', /* Otros datos necesarios */ });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});














//DELETE

// Endpoint para eliminar una vacuna por su ID
app.delete('/api/vacunas/:id', async (req, res) => {
  console.log('Solicitud para eliminar una vacuna por ID recibida');

  const vacunaId = req.params.id;
  console.log(`Solicitud de eliminación de vacuna para el ID: ${vacunaId}`);

  try {
    // Realiza una consulta para eliminar la vacuna por su ID
    const deleteVacunaQuery = 'DELETE FROM vacunas WHERE id = $1;';
    const result = await pool.query(deleteVacunaQuery, [vacunaId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Vacuna no encontrada para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que la vacuna ha sido eliminada con éxito
    res.status(200).json({ message: 'Vacuna eliminada con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para eliminar un diagnóstico por su ID
app.delete('/api/diagnostico/:id', async (req, res) => {
  console.log('Solicitud para eliminar un diagnóstico por ID recibida');

  const diagnosticoId = req.params.id;
  console.log(`Solicitud de eliminación de diagnóstico para el ID: ${diagnosticoId}`);

  try {
    // Realiza una consulta para eliminar el diagnóstico por su ID
    const deleteDiagnosticoQuery = 'DELETE FROM diagnostico WHERE id = $1;';
    const result = await pool.query(deleteDiagnosticoQuery, [diagnosticoId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Diagnóstico no encontrado para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que el diagnóstico ha sido eliminado con éxito
    res.status(200).json({ message: 'Diagnóstico eliminado con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para eliminar un tratamiento por su ID
app.delete('/api/tratamiento/:id', async (req, res) => {
  console.log('Solicitud para eliminar un tratamiento por ID recibida');

  const tratamientoId = req.params.id;
  console.log(`Solicitud de eliminación de tratamiento para el ID: ${tratamientoId}`);

  try {
    // Realiza una consulta para eliminar el tratamiento por su ID
    const deleteTratamientoQuery = 'DELETE FROM tratamiento WHERE id = $1;';
    const result = await pool.query(deleteTratamientoQuery, [tratamientoId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Tratamiento no encontrado para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que el tratamiento ha sido eliminado con éxito
    res.status(200).json({ message: 'Tratamiento eliminado con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para eliminar un nivel de acceso por su ID
app.delete('/api/nivel_acceso/:id', async (req, res) => {
  console.log('Solicitud para eliminar un nivel de acceso por ID recibida');

  const nivelAccesoId = req.params.id;
  console.log(`Solicitud de eliminación de nivel de acceso para el ID: ${nivelAccesoId}`);

  try {
    // Realiza una consulta para eliminar el nivel de acceso por su ID
    const deleteNivelAccesoQuery = 'DELETE FROM nivel_acceso WHERE id = $1;';
    const result = await pool.query(deleteNivelAccesoQuery, [nivelAccesoId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Nivel de acceso no encontrado para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que el nivel de acceso ha sido eliminado con éxito
    res.status(200).json({ message: 'Nivel de acceso eliminado con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para eliminar un cliente por DNI
app.delete('/api/cliente/:dni', async (req, res) => {
  console.log('Solicitud para eliminar un cliente por DNI recibida');

  const clienteDNI = req.params.dni;
  console.log(`Solicitud de eliminación de cliente para el DNI: ${clienteDNI}`);

  try {
    // Realiza una consulta para eliminar al cliente por su DNI
    const deleteClienteQuery = 'DELETE FROM cliente WHERE dni = $1;';
    const result = await pool.query(deleteClienteQuery, [clienteDNI]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Cliente no encontrado para el DNI proporcionado.' });
    }

    // Devuelve una respuesta indicando que el cliente ha sido eliminado con éxito
    res.status(200).json({ message: 'Cliente eliminado con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para eliminar un fabricante por su ID
app.delete('/api/fabricante/:id_fabricante', async (req, res) => {
  console.log('Solicitud para eliminar un fabricante por ID recibida');

  const fabricanteId = req.params.id_fabricante;
  console.log(`Solicitud de eliminación de fabricante para el ID: ${fabricanteId}`);

  try {
    // Realiza una consulta para eliminar al fabricante por su ID
    const deleteFabricanteQuery = 'DELETE FROM fabricante WHERE id_fabricante = $1;';
    const result = await pool.query(deleteFabricanteQuery, [fabricanteId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Fabricante no encontrado para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que el fabricante ha sido eliminado con éxito
    res.status(200).json({ message: 'Fabricante eliminado con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para eliminar un tipo de medicamento por su ID
app.delete('/api/tipomedicamento/:id', async (req, res) => {
  console.log('Solicitud para eliminar un tipo de medicamento por ID recibida');

  const tipoMedicamentoId = req.params.id;
  console.log(`Solicitud de eliminación de tipo de medicamento para el ID: ${tipoMedicamentoId}`);

  try {
    // Realiza una consulta para eliminar el tipo de medicamento por su ID
    const deleteTipoMedicamentoQuery = 'DELETE FROM tipomedicamento WHERE id = $1;';
    const result = await pool.query(deleteTipoMedicamentoQuery, [tipoMedicamentoId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Tipo de medicamento no encontrado para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que el tipo de medicamento ha sido eliminado con éxito
    res.status(200).json({ message: 'Tipo de medicamento eliminado con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para eliminar un usuario y sus registros relacionados
app.delete('/api/usuarios/:id_usuarios', async (req, res) => {
  console.log('Solicitud para eliminar un usuario por ID recibida');

  const usuarioId = req.params.id_usuarios;
  console.log(`Solicitud de eliminación de usuario para el ID: ${usuarioId}`);

  try {
    // Eliminar registros en fechas_vacaciones relacionados con el usuario
    const deleteFechasVacacionesQuery = 'DELETE FROM fechas_vacaciones WHERE medico_id = $1;';
    await pool.query(deleteFechasVacacionesQuery, [usuarioId]);

    // Eliminar registros en historial_medico relacionados con el usuario
    const deleteHistorialMedicoQuery = 'DELETE FROM historial_medico WHERE usuario_id = $1;';
    await pool.query(deleteHistorialMedicoQuery, [usuarioId]);

    // Eliminar registros en cliente_medicamentos relacionados con el usuario
    const deleteClienteMedicamentosQuery = 'DELETE FROM cliente_medicamentos WHERE usuario_id = $1;';
    await pool.query(deleteClienteMedicamentosQuery, [usuarioId]);

    // Eliminar citas relacionadas con el usuario
    const deleteCitasQuery = 'DELETE FROM citas WHERE medico_id = $1;';
    await pool.query(deleteCitasQuery, [usuarioId]);

    // Eliminar horarios médicos relacionados con el usuario
    const deleteHorariosMedicosQuery = 'DELETE FROM horarios_medicos WHERE medico_id = $1;';
    await pool.query(deleteHorariosMedicosQuery, [usuarioId]);

    // Luego, elimina al usuario
    const deleteUsuarioQuery = 'DELETE FROM usuarios WHERE id_usuarios = $1;';
    const result = await pool.query(deleteUsuarioQuery, [usuarioId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Usuario no encontrado para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que el usuario y sus registros relacionados han sido eliminados con éxito
    res.status(200).json({ message: 'Usuario y registros relacionados eliminados con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para eliminar un horario médico por su ID
app.delete('/api/horarios_medicos/:id', async (req, res) => {
  console.log('Solicitud para eliminar un horario médico por ID recibida');

  const horarioMedicoId = req.params.id;
  console.log(`Solicitud de eliminación de horario médico para el ID: ${horarioMedicoId}`);

  try {
    // Realiza una consulta para eliminar el horario médico por su ID
    const deleteHorarioMedicoQuery = 'DELETE FROM horarios_medicos WHERE id = $1;';
    const result = await pool.query(deleteHorarioMedicoQuery, [horarioMedicoId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Horario médico no encontrado para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que el horario médico ha sido eliminado con éxito
    res.status(200).json({ message: 'Horario médico eliminado con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para eliminar un laboratorio por su ID
app.delete('/api/laboratorio/:id', async (req, res) => {
  console.log('Solicitud para eliminar un laboratorio por ID recibida');

  const laboratorioId = req.params.id;
  console.log(`Solicitud de eliminación de laboratorio para el ID: ${laboratorioId}`);

  try {
    // Realiza una consulta para eliminar el laboratorio por su ID
    const deleteLaboratorioQuery = 'DELETE FROM laboratorio WHERE id = $1;';
    const result = await pool.query(deleteLaboratorioQuery, [laboratorioId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Laboratorio no encontrado para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que el laboratorio ha sido eliminado con éxito
    res.status(200).json({ message: 'Laboratorio eliminado con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para eliminar una fecha de vacaciones por su ID
app.delete('/api/fechas_vacaciones/:id', async (req, res) => {
  console.log('Solicitud para eliminar una fecha de vacaciones por ID recibida');

  const fechaVacacionesId = req.params.id;
  console.log(`Solicitud de eliminación de fecha de vacaciones para el ID: ${fechaVacacionesId}`);

  try {
    // Realiza una consulta para eliminar la fecha de vacaciones por su ID
    const deleteFechaVacacionesQuery = 'DELETE FROM fechas_vacaciones WHERE id = $1;';
    const result = await pool.query(deleteFechaVacacionesQuery, [fechaVacacionesId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Fecha de vacaciones no encontrada para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que la fecha de vacaciones ha sido eliminada con éxito
    res.status(200).json({ message: 'Fecha de vacaciones eliminada con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para eliminar un medicamento y sus registros relacionados
app.delete('/api/medicamentos/:id', async (req, res) => {
  console.log('Solicitud para eliminar un medicamento por ID recibida');

  const medicamentoId = req.params.id;
  console.log(`Solicitud de eliminación de medicamento para el ID: ${medicamentoId}`);

  try {
    // Eliminar registros en cliente_medicamentos relacionados con el medicamento
    const deleteClienteMedicamentosQuery = 'DELETE FROM cliente_medicamentos WHERE medicamento_id = $1;';
    await pool.query(deleteClienteMedicamentosQuery, [medicamentoId]);

    // Luego, elimina el medicamento
    const deleteMedicamentoQuery = 'DELETE FROM medicamento WHERE id = $1;';
    const result = await pool.query(deleteMedicamentoQuery, [medicamentoId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Medicamento no encontrado para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que el medicamento y sus registros relacionados han sido eliminados con éxito
    res.status(200).json({ message: 'Medicamento y registros relacionados eliminados con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para eliminar una cita por su ID
app.delete('/api/citas/:id', async (req, res) => {
  console.log('Solicitud para eliminar una cita por ID recibida');

  const citaId = req.params.id;
  console.log(`Solicitud de eliminación de cita para el ID: ${citaId}`);

  try {
    // Realiza una consulta para eliminar la cita por su ID
    const deleteCitaQuery = 'DELETE FROM citas WHERE id = $1;';
    const result = await pool.query(deleteCitaQuery, [citaId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Cita no encontrada para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que la cita ha sido eliminada con éxito
    res.status(200).json({ message: 'Cita eliminada con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});

// Endpoint para eliminar un registro en cliente_medicamentos por su ID
app.delete('/api/cliente_medicamentos/:id', async (req, res) => {
  console.log('Solicitud para eliminar un registro en cliente_medicamentos por ID recibida');

  const clienteMedicamentoId = req.params.id;
  console.log(`Solicitud de eliminación de registro en cliente_medicamentos para el ID: ${clienteMedicamentoId}`);

  try {
    // Realiza una consulta para eliminar el registro en cliente_medicamentos por su ID
    const deleteClienteMedicamentoQuery = 'DELETE FROM cliente_medicamentos WHERE id = $1;';
    const result = await pool.query(deleteClienteMedicamentoQuery, [clienteMedicamentoId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Registro en cliente_medicamentos no encontrado para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que el registro en cliente_medicamentos ha sido eliminado con éxito
    res.status(200).json({ message: 'Registro en cliente_medicamentos eliminado con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});


// Endpoint para eliminar un registro en historial_medico por su ID
app.delete('/api/historial_medico/:id', async (req, res) => {
  console.log('Solicitud para eliminar un registro en historial_medico por ID recibida');

  const historialMedicoId = req.params.id;
  console.log(`Solicitud de eliminación de registro en historial_medico para el ID: ${historialMedicoId}`);

  try {
    // Realiza una consulta para eliminar el registro en historial_medico por su ID
    const deleteHistorialMedicoQuery = 'DELETE FROM historial_medico WHERE id = $1;';
    const result = await pool.query(deleteHistorialMedicoQuery, [historialMedicoId]);

    // Verifica si se eliminó algún registro
    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Registro en historial_medico no encontrado para el ID proporcionado.' });
    }

    // Devuelve una respuesta indicando que el registro en historial_medico ha sido eliminado con éxito
    res.status(200).json({ message: 'Registro en historial_medico eliminado con éxito' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error en el servidor', error: error.message });
  }
});
































//VALIDADORES 







ValidateSpanishID = (function() {
  'use strict';
  
  var DNI_REGEX = /^(\d{8})([A-Z])$/;
  var CIF_REGEX = /^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/;
  var NIE_REGEX = /^[XYZ]\d{7,8}[A-Z]$/;

  var ValidateSpanishID = function( str ) {

    // Ensure upcase and remove whitespace
    str = str.toUpperCase().replace(/\s/, '');

    var valid = false;
    var type = spainIdType( str );

    switch (type) {
      case 'dni':
        valid = validDNI( str );
        break;
      case 'nie':
        valid = validNIE( str );
        break;
      case 'cif':
        valid = validCIF( str );
        break;
    }

    return {
      type: type,
      valid: valid
    };

  };

  var spainIdType = function( str ) {
    if ( str.match( DNI_REGEX ) ) {
      return 'dni';
    }
    if ( str.match( CIF_REGEX ) ) {
      return 'cif';
    }
    if ( str.match( NIE_REGEX ) ) {
      return 'nie';
    }
  };

  var validDNI = function( dni ) {
    var dni_letters = "TRWAGMYFPDXBNJZSQVHLCKE";
    var letter = dni_letters.charAt( parseInt( dni, 10 ) % 23 );
    
    return letter == dni.charAt(8);
  };

  var validNIE = function( nie ) {

    // Change the initial letter for the corresponding number and validate as DNI
    var nie_prefix = nie.charAt( 0 );

    switch (nie_prefix) {
      case 'X': nie_prefix = 0; break;
      case 'Y': nie_prefix = 1; break;
      case 'Z': nie_prefix = 2; break;
    }

    return validDNI( nie_prefix + nie.substr(1) );

  };

  var validCIF = function( cif ) {

    var match = cif.match( CIF_REGEX );
    var letter  = match[1],
        number  = match[2],
        control = match[3];

    var even_sum = 0;
    var odd_sum = 0;
    var n;

    for ( var i = 0; i < number.length; i++) {
      n = parseInt( number[i], 10 );

      // Odd positions (Even index equals to odd position. i=0 equals first position)
      if ( i % 2 === 0 ) {
        // Odd positions are multiplied first.
        n *= 2;

        // If the multiplication is bigger than 10 we need to adjust
        odd_sum += n < 10 ? n : n - 9;

      // Even positions
      // Just sum them
      } else {
        even_sum += n;
      }

    }

    var control_digit = (even_sum + odd_sum) % 10 !== 0 ? 10 - ((even_sum + odd_sum) % 10) : 0;
    var control_letter = 'JABCDEFGHI'.substr( control_digit, 1 );

    // Control must be a digit
    if ( letter.match( /[ABEH]/ ) ) {
      return control == control_digit;

    // Control must be a letter
    } else if ( letter.match( /[KPQS]/ ) ) {
      return control == control_letter;

    // Can be either
    } else {
      return control == control_digit || control == control_letter;
    }

  };

  return ValidateSpanishID;
})();
app.listen(PORT, () => {
  console.log(`Servidor escuchando en el puerto ${PORT}`);
});


function validarNumeroColegiadoNacional(numeroColegiado) {
  // Convertir numeroColegiado a cadena
  const numeroColegiadoStr = numeroColegiado.toString();

  const errors = {};

  // Verificar que el número de colegiado tiene 9 dígitos
  if (!/^\d{9}$/.test(numeroColegiadoStr)) {
    errors.lengthError = 'El número de colegiado debe tener 9 dígitos';
  }

  // Extraer los componentes del número de colegiado
  const codigoActual = numeroColegiadoStr.slice(0, 2);
  const codigoPrimeraColegiacion = numeroColegiadoStr.slice(2, 4);
  const numeroCorrelativo = numeroColegiadoStr.slice(4);

  // Verificar que los códigos de colegio sean números
  if (!/^\d{2}$/.test(codigoActual)) {
    errors.codigoActualError = 'El código actual del colegio debe ser un número de 2 dígitos';
  }

  if (!/^\d{2}$/.test(codigoPrimeraColegiacion)) {
    errors.codigoPrimeraColegiacionError = 'El código de primera colegiación debe ser un número de 2 dígitos';
  }

  // Verificar que el número correlativo sea una cadena numérica de 5 dígitos
  if (!/^\d{5}$/.test(numeroCorrelativo)) {
    errors.numeroCorrelativoError = 'El número correlativo debe ser una cadena numérica de 5 dígitos';
  }

  // Si hay errores, devolver el objeto con los mensajes de error
  if (Object.keys(errors).length > 0) {
    return { isValid: false, errors };
  }

  // Si no hay errores, el número de colegiado es válido
  return { isValid: true };
}



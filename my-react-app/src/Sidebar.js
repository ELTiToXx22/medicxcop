import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './Sidebar.css';

import {Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import axios from "axios";

const Sidebar = ({ isLoggedIn, userRole, usuario, tableHistorial, onToggleTableHistorial , onToggleTableCliente , tableClientes }) => {
    const [modalTratamiento, setModalTratamiento] = useState(false);
    const [modalClienteAlta, setModalClienteAlta] = useState(false);
    const [selectedImage, setSelectedImage] = useState(null);
    const [errorMessage, setErrorMessage] = useState('');
    const [localTableHistorial, setLocalTableHistorial] = useState(tableHistorial);
    const [localTableCliente, setLocalTableCliente] = useState(tableClientes);
    const [tratamientos, setTratamientos] = useState([]);
    const [diagnosticos, setDiagnosticos] = useState([]);
    const [vacunas, setVacunas] = useState([]);




    const [formData, setFormData] = useState({
        nombre: '',
        Primer_Apellido: '',
        cliente_id: '',
        nrregistro: '',
        dosis: 0,
        fecha_inicio: '',
        fecha_fin: '',
        tratamiento:'',
        diagnostico:'',
        notas:'',
        vacuna:'',
        dni: '',
        correo_electronico: '',
        contrasena: '',
        nombrecl: '',
        primer_apellido: '',
        segundo_apellido: '',
        fecha_nacimiento: '',
        genero: '',
        telefono: '',
        direccion: '',
        codigo_postal: '',
    });

    useEffect(() => {
        // console.log('Rol del usuario en Sidebar:', userRole);
    }, [userRole]);

    const toggleNuevoTratamiento = () => {
        setModalTratamiento(!modalTratamiento);

        // Restablecer el formulario cuando se abra o cierre el modal
        if (!modalTratamiento) {
            setSelectedImage(null);
            setErrorMessage('');
            setFormData({
                dni: '',
                correo_electronico: '',
                contrasena: '',
                nombrecl: '',
                primer_apellido: '',
                segundo_apellido: '',
                fecha_nacimiento: '',
                genero: '',
                telefono: '',
                direccion: '',
                codigo_postal: '',
            });
        }
    };

    const toggleClienteAlta = () => {
        setModalClienteAlta(!modalClienteAlta);

        // Restablecer el formulario cuando se abra o cierre el modal
        if (!modalClienteAlta) {
            setFormData({
                dni: '',
                correo_electronico: '',
                contrasena: '',
                nombrecl: '',
                primer_apellido: '',
                segundo_apellido: '',
                fecha_nacimiento: '',
                genero: '',
                telefono: '',
                direccion: '',
                codigo_postal: '',
            });
        }
    };
    const [isSidebarOpen, setIsSidebarOpen] = useState(true);

    const toggleSidebar = () => {
        setIsSidebarOpen(!isSidebarOpen);
    };

    const handleImageSelect = (event) => {
        const file = event.target.files[0];
        if (file) {
            const fileType = file.type;
            if (fileType === 'image/jpeg' || fileType === 'image/png') {
                setSelectedImage(URL.createObjectURL(file));
                setErrorMessage('');
            } else {
                setErrorMessage('Por favor, selecciona una imagen JPG o PNG.');
                setSelectedImage(null);
            }
        }
    };

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setFormData({ ...formData,  [name]: value });
    };

    const handleRegister = (usuario) => {
        if (!isLoggedIn) {
            // El usuario no está logeado, muestra un mensaje o toma alguna acción.
            alert('Debes iniciar sesión para registrar un nuevo tratamiento.');
            return;
        }
        console.log('userRole:', userRole);
        // Comprobar el rol del usuario antes de permitir el registro
        if (userRole !== 1 && userRole !== 2 && userRole !== 3) {
            alert(`No tienes permiso para registrar un nuevo tratamiento como ${userRole}.`);
            return;
        }
        console.log('usuario:', usuario);

        const requiredFields = ['cliente_id', 'nrregistro', 'dosis', 'fecha_inicio', 'tratamiento', 'diagnostico', 'vacuna'];
        const emptyFields = requiredFields.filter(field => !formData[field]);

        if (emptyFields.length > 0) {
            alert(`Por favor, completa los campos requeridos: ${emptyFields.join(', ')}.`);
            return;
        }

        // Comprobar si los valores en formData son válidos antes de crear el objeto data
        if (!formData.cliente_id) {
            alert('Por favor, completa el campo cliente_id.');
            return;
        }

        if (!formData.nrregistro || isNaN(parseFloat(formData.dosis))) {
            console.log('nrregistro', formData.nrregistro);
            console.log('dosis', formData.dosis);
            alert('Por favor, completa el campo nrregistro y asegúrate de que el valor de dosis sea válido.');
            return;
        }

        const data = {
            cliente_id: formData.cliente_id,
            usuario: usuario, // Usa el valor de usuario directamente del componente
            nrregistro: formData.nrregistro,
            dosis: formData.dosis,
            fecha_ini: formData.fecha_inicio, // Asegúrate de usar la misma capitalización
            fecha_fin: formData.fecha_fin,
            tratamiento: formData.tratamiento,
            diagnostico:formData.diagnostico,
            notas:formData.notas,
            vacuna:formData.vacuna
        };

        console.log('Datos en el objeto data:', data);

        // Realizar una solicitud POST al servidor
        axios.post('http://localhost:3000/api/cliente_medicamentos', data)
            .then((response) => {
                console.log('Respuesta de la API:', response.data);
                toggleNuevoTratamiento(); // Cerrar el modal
            })
            .catch((error) => {
                console.error('Error al llamar a la API:', error);
                console.error('Detalles de la respuesta del servidor:', error.response);
                alert('Error al registrar. Por favor, inténtalo de nuevo.');
            });

    };

    const handleRegisterCliente = async () => {
        // Agregar lógica para validar y registrar el cliente aquí
        const requiredFields = ['dni', 'correo_electronico', 'contrasena', 'nombrecl', 'primer_apellido', 'fecha_nacimiento'];
        const emptyFields = requiredFields.filter((field) => !formData[field]);

        if (emptyFields.length > 0) {
            alert(`Por favor, completa los campos requeridos: ${emptyFields.join(', ')}.`);
            return;
        }

        const data = {
            dni: formData.dni,
            correo_electronico: formData.correo_electronico,
            contrasena: formData.contrasena,
            nombre: formData.nombrecl,
            primer_apellido: formData.primer_apellido,
            segundo_apellido: formData.segundo_apellido,
            fecha_nacimiento: formData.fecha_nacimiento,
            genero: formData.genero,
            telefono: formData.telefono,
            direccion: formData.direccion,
            codigo_postal: formData.codigo_postal,
        };

        console.log('Datos en el objeto data:', data);

        try {
            // Realizar una solicitud POST al servidor para registrar el cliente
            const response = await axios.post('http://localhost:3000/api/register_cliente', data);
            console.log('Respuesta de la API:', response.data);

            // Solo si la solicitud es exitosa, activar la función para toggleClienteAlta
            toggleClienteAlta();
        } catch (error) {
            console.error('Error al llamar a la API:', error);

            if (error.response) {
                // El servidor ha respondido con un código de estado diferente de 2xx
                const { status, data } = error.response;
                if (status === 400) {
                    // Error de validación u otro tipo de error del lado del cliente
                    alert(`Error de validación: ${data.message}`);
                } else if (status === 401) {
                    // Error de autenticación, por ejemplo, si no está autorizado para realizar la acción
                    alert('Error de autenticación: No está autorizado para realizar esta acción.');
                } else {
                    // Otro tipo de error del servidor
                    alert('Error del servidor. Por favor, inténtalo de nuevo más tarde.');
                }
            } else if (error.request) {
                // La solicitud fue realizada, pero no se recibió una respuesta del servidor
                alert('No se recibió respuesta del servidor. Por favor, inténtalo de nuevo más tarde.');
            } else {
                // Otros tipos de errores
                alert('Error inesperado. Por favor, inténtalo de nuevo.');
            }
        }
    };




    const handleToggleTableHistorial = () => {
        if (isLoggedIn){
            onToggleTableHistorial();

            // Oculta la otra tabla si está visible
            if (localTableCliente) {
                onToggleTableCliente();
                setLocalTableCliente(false);
            }

            setLocalTableHistorial(!localTableHistorial);
        }
    };

    const handleToggleTableCliente = () => {
        if (isLoggedIn){
            onToggleTableCliente();

            // Oculta la otra tabla si está visible
            if (localTableHistorial) {
                onToggleTableHistorial();
                setLocalTableHistorial(false);
            }

            setLocalTableCliente(!localTableCliente);
        }

    };


    useEffect(() => {
        axios.get('http://localhost:3000/api/tratamientos')
            .then((response) => {
                setTratamientos(response.data.tratamientos || []);
            })
            .catch((error) => {
                console.error('Error al obtener tratamientos:', error);
            });
    }, []);


    useEffect(() => {
        axios.get('http://localhost:3000/api/diagnosticos')
            .then((response) => {
                setDiagnosticos(response.data.diagnosticos || []);
            })
            .catch((error) => {
                console.error('Error al obtener diagnósticos:', error);
            });
    }, []);

    useEffect(() => {
        axios.get('http://localhost:3000/api/vacunas')
            .then((response) => {
                setVacunas(response.data.vacunas);
            })
            .catch((error) => {
                console.error('Error al obtener vacunas:', error);
            });
    }, []);


    return (
        <nav id="sidebar" className={isSidebarOpen ? 'active' : ''}>
            <div className="custom-menu">
                <button type="button" id="sidebarCollapse" className="btn btn-primary" onClick={toggleSidebar}>
                    <i className="fa fa-bars"></i>
                    <span className="sr-only">Toggle Sidebar</span>
                </button>
            </div>
            <div className="p-4">
                <h1><a className="logo" >MEDICX</a></h1>
                <ul className="list-unstyled components mb-5">
                    <li className="active">
                    </li>

                    <li>
                        <button type="button" className="custom-btn" onClick={handleToggleTableHistorial}>
                            <span>Historial Medico</span>
                        </button>
                    </li>

                    <li>
                        <button type="button" className="custom-btn" onClick={handleToggleTableCliente}>
                            <span>Cliente Clinica</span>
                        </button>
                    </li>

                    <li>
                        <button type="button" className="custom-btn" onClick={toggleNuevoTratamiento}>
                            <span>Nuevo Tratamiento</span>
                        </button>
                    </li>

                    <li>
                        <button type="button" className="custom-btn" onClick={toggleClienteAlta}>
                            <span>Dar de alta cliente</span>
                        </button>
                    </li>

                </ul>

                <div className="mb-5"></div>

                <div className="footer"></div>
            </div>

            {/* Modal */}
            <Modal isOpen={modalTratamiento} toggle={toggleNuevoTratamiento}>
                <ModalHeader toggle={toggleNuevoTratamiento}>Nuevo Tratamiento</ModalHeader>
                <ModalBody>
                    <div>
                        <input type="file" accept=".jpg, .jpeg, .png" onChange={handleImageSelect} />
                    </div>
                    {errorMessage && <div className="text-danger">{errorMessage}</div>}
                    {selectedImage && <img src={selectedImage} alt="Selected" style={{ maxWidth: '100%', height: '100px' }}/>}


                    <div className="form-group">
                        <label htmlFor="cliente_id">DNI:</label>
                        <input type="text" id="cliente_id" name="cliente_id" value={formData.cliente_id} onChange={handleInputChange} className="form-control" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="nrregistro">Numero de Registro:</label>
                        <input type="number" id="nrregistro" name="nrregistro" value={formData.nrregistro} onChange={handleInputChange} className="form-control"/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="dosis">dosis:</label>
                        <input type="number" id="dosis" name="dosis" value={formData.dosis} onChange={handleInputChange} className="form-control"/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="fecha_inicio">Fecha inicio:</label>
                        <input type="date" id="fecha_inicio" name="fecha_inicio" value={formData.fecha_inicio} onChange={handleInputChange} className="form-control"/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="fecha_fin">Fecha fin:</label>
                        <input type="date" id="fecha_fin" name="fecha_fin" value={formData.fecha_fin} onChange={handleInputChange} className="form-control"/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="diagnostico">Diagnóstico:</label>
                        <select id="diagnostico" name="diagnostico" value={formData.diagnostico} onChange={handleInputChange} className="form-control">
                            <option value="">Selecciona un diagnóstico</option>
                            {diagnosticos.map((diagnostico) => (
                                <option key={diagnostico.id} value={diagnostico.descripcion}>
                                    {diagnostico.descripcion}
                                </option>
                            ))}
                        </select>
                    </div>

                    <div>
                        <div className="form-group">
                            <label htmlFor="tratamiento">Tratamiento:</label>
                            <select id="tratamiento" name="tratamiento" value={formData.tratamiento} onChange={handleInputChange} className="form-control">
                                <option value="">Selecciona un tratamiento</option>
                                {tratamientos.map((tratamiento) => (
                                    <option key={tratamiento.id} value={tratamiento.descripcion}>
                                        {tratamiento.descripcion}
                                    </option>
                                ))}
                            </select>
                        </div>
                    </div>



                    <div className="form-group">
                        <label htmlFor="notas">Notas:</label>
                        <input type="text" id="notas" name="notas" value={formData.notas} onChange={handleInputChange} className="form-control" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="vacuna">Vacuna:</label>
                        <select id="vacuna" name="vacuna" value={formData.vacuna} onChange={handleInputChange} className="form-control">
                            <option value="">Selecciona una vacuna</option>
                            {vacunas.map((vacuna) => (
                                <option key={vacuna.id} value={vacuna.nombre}>
                                    {vacuna.nombre}
                                </option>
                            ))}
                        </select>
                    </div>

                </ModalBody>
                <ModalFooter>
                    <Button onClick={() => handleRegister(usuario)} style={{ backgroundColor: '#3445B4', border: 'none' }}>GUARDAR</Button>
                    <Button color="secondary" onClick={toggleNuevoTratamiento}>CERRAR</Button>
                </ModalFooter>
            </Modal>



            <Modal isOpen={modalClienteAlta} toggle={toggleClienteAlta}>
                <ModalHeader toggle={toggleClienteAlta}>Nuevo Cliente</ModalHeader>
                <ModalBody>
                    <div className="form-group">
                        <label htmlFor="dni">DNI:</label>
                        <input type="text" id="dni" name="dni" value={formData.dni} onChange={handleInputChange} className="form-control" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="correo_electronico">Correo Electrónico:</label>
                        <input type="email" id="correo_electronico" name="correo_electronico" value={formData.correo_electronico} onChange={handleInputChange} className="form-control" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="contrasena">Contraseña:</label>
                        <input type="password" id="contrasena" name="contrasena" value={formData.contrasena} onChange={handleInputChange} className="form-control" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="nombrecl">Nombre:</label>
                        <input type="text" id="nombrecl" name="nombrecl" value={formData.nombrecl} onChange={handleInputChange} className="form-control" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="primer_apellido">Primer Apellido:</label>
                        <input type="text" id="primer_apellido" name="primer_apellido" value={formData.primer_apellido} onChange={handleInputChange} className="form-control" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="segundo_apellido">Segundo Apellido:</label>
                        <input type="text" id="segundo_apellido" name="segundo_apellido" value={formData.segundo_apellido} onChange={handleInputChange} className="form-control" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="fecha_nacimiento">Fecha de Nacimiento:</label>
                        <input type="date" id="fecha_nacimiento" name="fecha_nacimiento" value={formData.fecha_nacimiento} onChange={handleInputChange} className="form-control" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="genero">Género:</label>
                        <select id="genero" name="genero" value={formData.genero} onChange={handleInputChange} className="form-control">
                            <option value="">Selecciona un género</option>
                            <option value="Hombre">Hombre</option>
                            <option value="Mujer">Mujer</option>
                        </select>
                    </div>

                    <div className="form-group">
                        <label htmlFor="telefono">Teléfono:</label>
                        <input type="text" id="telefono" name="telefono" value={formData.telefono} onChange={handleInputChange} className="form-control" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="direccion">Dirección:</label>
                        <input type="text" id="direccion" name="direccion" value={formData.direccion} onChange={handleInputChange} className="form-control" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="codigo_postal">Código Postal:</label>
                        <input type="text" id="codigo_postal" name="codigo_postal" value={formData.codigo_postal} onChange={handleInputChange} className="form-control" />
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button onClick={() => handleRegisterCliente()} style={{ backgroundColor: '#3445B4', border: 'none' }}>
                        GUARDAR
                    </Button>
                    <Button color="secondary" onClick={toggleClienteAlta}>
                        CERRAR
                    </Button>
                </ModalFooter>
            </Modal>
        </nav>
    );
}

export default Sidebar;

import React from 'react';
import './App.css';
import Sidebar from './Sidebar';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Input, Label, Alert} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.css';
import Table from 'react-bootstrap/Table';
import axios from 'axios';
import '@fortawesome/fontawesome-free/css/all.css';

class App extends React.Component {
    state = {
        abierto: false,                 // Estado del modal principal
        correo_electronico: '',         // Valor del campo de correo electrónico
        contrasena: '',                 // Valor del campo de contraseña
        nivel_acceso: '',               // Nivel de acceso del usuario
        userRole: '',                   // Rol del usuario
        emailError: '',                 // Mensaje de error para el campo de correo electrónico
        passwordError: '',              // Mensaje de error para el campo de contraseña
        showPassword: false,            // Estado de visibilidad de la contraseña
        isLoggedIn: false,              // Estado de inicio de sesión del usuario
        showSecondModal: false,         // Estado del segundo modal
        registroAbierto: false,         // Estado del modal de registro
        showSuccessAlert: false,        // Estado de la alerta de éxito
        showUserExistsAlert: false,     // Estado de la alerta de usuario existente
        registrationButtonVisible: false, // Visibilidad del botón de registro
        num_colegiado: '',              // Valor del campo de número de colegiado
        historialesMedicos: [],         // Lista de historiales médicos
        tableHistorial: false,          // Estado de visibilidad de la tabla de historiales
        searchValue: '',                // Valor del campo de búsqueda
        nombre: '',                     // Valor del campo de nombre
        primer_apellido: '',            // Valor del campo de primer apellido
        segundo_apellido: '',           // Valor del campo de segundo apellido
        confirmarContrasena: '',        // Valor del campo de confirmación de contraseña
        confirmPasswordError: '',       // Mensaje de error para la confirmación de contraseña
        historialClientes: [],                   // Lista de clientes
        tableCliente: false,            // Estado de visibilidad de la tabla de cliente
        opcionesNivelAcceso: [],
    }

    // Funciones de apertura y cierre de modales
    abrirModal = () => {
        // Cambia el estado para abrir o cerrar el modal principal
        this.setState({abierto: !this.state.abierto});
    }

    cerrarModal = () => {
        // Restablece el estado al cerrar el modal principal
        this.setState({
            abierto: false,
            correo_electronico: '',
            contrasena: '',
            emailError: '',
            passwordError: '',
            showPassword: false,
            num_colegiado: '',
            nombre: '',
            primer_apellido: '',
            segundo_apellido: '',
            confirmarContrasena: '',
            confirmPasswordError: '',
        });
    };

    cerrarModalRegistro = () => {
        // Restablece el estado al cerrar el modal de registro
        this.setState({
            correo_electronico: '',
            contrasena: '',
            emailError: '',
            passwordError: '',
            showPassword: false,
            registroAbierto: false,
            showSuccessAlert: false,
            showUserExistsAlert: false,
            num_colegiado: '',
            nombre: '',
            primer_apellido: '',
            segundo_apellido: '',
            confirmarContrasena: '',
            confirmPasswordError: '',
        });
    };

    // Abre el modal de registro estableciendo el estado de registroAbierto a true
    abrirModalRegistro = () => {
        this.setState({registroAbierto: true});
    };

    // Abre el segundo modal estableciendo el estado de showSecondModal a true
    openSecondModal = () => {
        this.setState({showSecondModal: true});
    };

    // Maneja el cierre de sesión del usuario
    handleLogout = () => {
        // Actualiza el estado para indicar que el usuario no está autenticado
        this.setState({isLoggedIn: false});
        // Cierra el segundo modal
        this.setState({showSecondModal: false});
        // Oculta el botón de registro
        this.setState({registrationButtonVisible: false});
    };

    // Cierra el segundo modal estableciendo el estado de showSecondModal a false
    closeSecondModal = () => {
        this.setState({showSecondModal: false});
    };

    // Funciones para gestionar el inicio de sesión
    handleUsernameChange = (event) => {
        this.setState({correo_electronico: event.target.value});
    };

    // Actualiza el estado de la contraseña durante la entrada del usuario
    handlePasswordChange = (event) => {
        this.setState({contrasena: event.target.value});
    };

    handleConfirmPasswordChange = (event) => {
        this.setState({confirmarContrasena: event.target.value});
    };

    handleConfirmPasswordBlur = () => {
        const {contrasena, confirmarContrasena} = this.state;

        if (contrasena !== confirmarContrasena) {
            this.setState({confirmPasswordError: 'Las contraseñas no coinciden'});
        } else {
            this.setState({confirmPasswordError: ''});
        }
    };

    handleNumColegiadoChange = (event) => {
        this.setState({num_colegiado: event.target.value});
    };

    handleNombreChange = (event) => {
        this.setState({nombre: event.target.value});
    };

    handlePrimerApellidoChange = (event) => {
        this.setState({primer_apellido: event.target.value});
    };

    handleSegundoApellidoChange = (event) => {
        this.setState({segundo_apellido: event.target.value});
    };

    handleTogglePasswordVisibility = () => {
        this.setState((prevState) => ({showPassword: !prevState.showPassword}));
    };

    handleEmailBlur = () => {
        const {correo_electronico} = this.state;
        const emailValid = this.isEmailValid(correo_electronico);
        this.setState({emailError: emailValid ? '' : 'Correo electrónico inválido'});
    };

    handlePasswordBlur = () => {
        const {contrasena} = this.state;
        const passwordValid = this.isPasswordValid(contrasena);
        this.setState({passwordError: passwordValid ? '' : 'Contraseña inválida'});
    };
    handleSelectChange = (event) => {
        this.setState({nivel_acceso: event.target.value});
    };

    handleKeyDown = (event) => {
        const {isLoggedIn} = this.state;
        if (isLoggedIn) {
            if (event.key === 'Enter') {
                this.handleSearchButtonClick();
            }
        }
    };

    handleSubmit1 = async (event) => {
        event.preventDefault();

        const {
            nombre,
            primer_apellido,
            segundo_apellido,
            correo_electronico,
            contrasena,
            confirmarContrasena,
            nivel_acceso,
            num_colegiado
        } = this.state;
        const emailValid = this.isEmailValid(correo_electronico);
        const passwordValid = this.isPasswordValid(contrasena);

        if (!correo_electronico || !contrasena || !nivel_acceso || !num_colegiado || !nombre || !primer_apellido || !segundo_apellido) {
            alert('Por favor, complete todos los campos.');
            return;
        }

        if (!emailValid || !passwordValid) {
            this.setState({
                emailError: !emailValid ? 'Correo electrónico inválido' : '',
                passwordError: !passwordValid ? 'Contraseña inválida' : ''
            });
            return;
        }

        if (contrasena !== confirmarContrasena) {
            this.setState({confirmPasswordError: 'Las contraseñas no coinciden'});
            return;
        }

        try {
            const response = await axios.post('http://localhost:3000/api/register', {
                nombre,
                primer_apellido,
                segundo_apellido,
                num_colegiado,
                correo_electronico,
                contrasena,
                nivel_acceso
            });

            console.log('Respuesta de la API:', response.data);

            if (response.data.message === 'Usuario registrado con éxito') {
                this.setState({showSuccessAlert: true});
                setTimeout(() => {
                    this.setState({showSuccessAlert: false});
                }, 3000);
            } else if (response.data.message === 'Usuario ya existe') {
                this.setState({showUserExistsAlert: true});
                setTimeout(() => {
                    this.setState({showUserExistsAlert: false});
                }, 3000);
            }
        } catch (error) {
            console.error('Error al llamar a la API:', error);
            if (error.response && error.response.status === 400) {
                this.setState({showUserExistsAlert: true});
                setTimeout(() => {
                    this.setState({showUserExistsAlert: false});
                }, 3000);
            }
        }
    };


    handleSubmit2 = (event) => {
        event.preventDefault();

        const {correo_electronico, contrasena} = this.state;

        if (!correo_electronico || !contrasena) {
            alert('Por favor, complete todos los campos.');
            return;
        }

        console.log('Solicitud de inicio de sesión enviada:', {correo_electronico, contrasena});

        // Hacer la solicitud POST a tu API para iniciar sesión
        axios.post('http://localhost:3000/api/login/usuarios', {correo_electronico, contrasena})
            .then((response) => {
                console.log('Respuesta de la API:', response.data);

                this.setState({
                    isLoggedIn: true,
                    correo_electronico: '',
                    contrasena: '',
                    emailError: '',
                    passwordError: '',
                    showPassword: false,
                    usuario: correo_electronico,
                    userRole: response.data.role
                });

                // Verificar si el usuario es administrador
                if (response.data.role === 1) {
                    this.setState({
                        registrationButtonVisible: true,
                    });
                } else {
                    this.setState({
                        registrationButtonVisible: false,
                    });
                }

                this.cerrarModal(); // Cierra la modal
            })
            .catch((error) => {
                console.error('Error al llamar a la API:', error);
                alert('Error al iniciar sesión. Por favor, verifica tus credenciales.');
            });
    };


    isEmailValid = (email) => {
        const emailRegex = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-lz0-9](?:[a-z0-9-]*[a-z0-9])?$/;
        return emailRegex.test(email);
    };

    isPasswordValid = (password) => {
        return password.length >= 8;
    };

    toggleTableHistorial = () => {
        this.setState((prevState) => ({
            tableHistorial: !prevState.tableHistorial,
        }));
    };

    // Método para cargar los datos del historial médico
    loadHistorialesMedicos = async () => {
        try {
            const response = await axios.get('http://localhost:3000/api/historial_medico');
            const historialesMedicos = response.data.historiales_medicos;
            this.setState({historialesMedicos});
        } catch (error) {
            console.error('Error al obtener historiales médicos:', error);
        }
    };

    toggleTableCliente = () => {
        this.setState((prevState) => ({
            tableCliente: !prevState.tableCliente,
        }));
    };

    // Método para cargar los datos del cliente
    loadhistorialClientes = async () => {
        try {
            const response = await axios.get('http://localhost:3000/api/historial_cliente');
            const historialClientes = response.data.historial_clientes;
            this.setState({historialClientes});
        } catch (error) {
            console.error('Error al obtener clientes:', error);
        }
    };

    async componentDidMount() {
        try {
            // Cargar datos del historial médico al montar el componente
            await this.loadHistorialesMedicos();
            await this.loadhistorialClientes();
            console.log('Historiales Médicos:', this.state.historialesMedicos);
            console.log('Historial Clientes:', this.state.historialClientes);

            // Cargar opciones de nivel de acceso
            const response = await axios.get('http://localhost:3000/api/nivel_acceso');
            const opcionesNivelAcceso = response.data.niveles_acceso;
            this.setState({ opcionesNivelAcceso });
            console.log('Opciones de Nivel de Acceso:', opcionesNivelAcceso);
        } catch (error) {
            if (error.message === 'Error al obtener historiales médicos') {
                console.error('Error al obtener historiales médicos:', error);
            } else if (error.message === 'Error al obtener historiales de clientes') {
                console.error('Error al obtener historiales de clientes:', error);
            } else if (error.message === 'Error al obtener opciones de nivel de acceso') {
                console.error('Error al obtener opciones de nivel de acceso:', error);
            } else {
                console.error('Error desconocido:', error);
            }
        }
    }


    handleSearchInputChange = (event) => {
        this.setState({searchValue: event.target.value});
    };


    handleSearchButtonClick = async () => {
        const { searchValue, isLoggedIn } = this.state;

        try {
            let response;

            // Verifica si searchValue tiene un valor antes de realizar la solicitud
            if (searchValue && searchValue.length > 0) {
                // Si searchValue tiene valor, realiza la solicitud solo para historiales_medicos
                response = await axios.get(`http://localhost:3000/api/historial_medico/${searchValue}`);
            } else {
                // Si searchValue no tiene valor, simplemente asigna un objeto vacío a 'data'
                response = { data: {} };
            }

            const data = response.data.historiales_medicos || response.data.historial_medico;
            this.setState({ searchValue: '' });

            // Actualiza el estado global de la aplicación con la información obtenida solo si isLoggedIn es true
            if (isLoggedIn) {
                this.setState({ data, tableHistorial: isLoggedIn }, () => {
                    console.log('Estado actualizado después de handleSearchButtonClick:', this.state);
                });
            }
        } catch (error) {
            console.error('Error al obtener historiales médicos:', error);
        }
    };





    render() {
        const {tableHistorial, historialesMedicos} = this.state;
        const {tableCliente,historialClientes } = this.state;

        const {
            nombre,
            primer_apellido,
            segundo_apellido,
            num_colegiado,
            abierto,
            correo_electronico,
            contrasena,
            confirmarContrasena,
            confirmPasswordError,
            emailError,
            passwordError,
            showPassword,
            isLoggedIn,

        } = this.state;

        const modalStyles = {
            position: "absolute",
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)'
        };

        return (
            <>
                <div className="search-box">
                    <button className="btn-search" onClick={this.handleSearchButtonClick}>
                        <i className="fas fa-search"></i>
                    </button>
                    <input type="text" className="input-search" placeholder="Type to Search..."
                           value={this.state.searchValue} onChange={this.handleSearchInputChange}
                           onKeyDown={this.handleKeyDown}/>
                </div>

                <div className="principal">

                    <div >


                        {tableHistorial && (
                            <Table>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>DNI Paciente</th>
                                    <th>Nombre</th>
                                    <th>Apellidos</th>
                                    <th>Num Colegiado</th>
                                    <th>Medico</th>
                                    <th>Fecha</th>
                                    <th>Vacunas</th>
                                    <th>Diagnóstico</th>
                                    <th>Tratamiento</th>
                                    <th>Notas</th>
                                </tr>
                                </thead>
                                <tbody>
                                {historialesMedicos.map((historial) => (
                                    <tr key={historial.id}>
                                        <td>{historial.id}</td>
                                        <td>{historial.cliente_id}</td>
                                        <td>{historial.nombre_cliente}</td>
                                        <td>{historial.apellidos_cliente}</td>
                                        <td>{historial.num_colegiado_usuario}</td>
                                        <td>{historial.nombre_usuario}</td>
                                        <td>{new Date(historial.fecha).toLocaleDateString()}</td>
                                        <td>{historial.descripcion_vacuna}</td>
                                        <td>{historial.descripcion_diagnostico}</td>
                                        <td>{historial.descripcion_tratamiento}</td>
                                        <td>{historial.notas}</td>
                                    </tr>
                                ))}
                                </tbody>
                            </Table>
                        )}

                    </div>

                    <div>
                        {tableCliente && (
                        <Table>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Primer Apellido</th>
                                <th>Segundo Apellido</th>
                                <th>Fecha de nacimiento </th>
                                <th>Genero</th>
                                <th>Telefono</th>
                                <th>Dirrecion</th>
                                <th>Cod. Postal</th>
                                <th>DNI</th>
                                <th>Correo Electronico</th>
                            </tr>
                            </thead>
                            <tbody>
                            {historialClientes.map((cliente) => (
                                <tr key={cliente.id}>
                                    <td>{cliente.id}</td>
                                    <td>{cliente.nombre}</td>
                                    <td>{cliente.primer_apellido}</td>
                                    <td>{cliente.segundo_apellido}</td>
                                    <td>{new Date(cliente.fecha_nacimiento).toLocaleDateString()}</td>
                                    <td>{cliente.genero}</td>
                                    <td>{cliente.telefono}</td>
                                    <td>{cliente.direccion}</td>
                                    <td>{cliente.codigo_postal}</td>
                                    <td>{cliente.dni}</td>
                                    <td>{cliente.correo_electronico}</td>


                                </tr>
                            ))}
                            </tbody>
                        </Table>
                        )}
                    </div>

                    <div className="secundario">
                        <Sidebar
                            isLoggedIn={isLoggedIn}
                            userRole={this.state.userRole}
                            usuario={this.state.usuario}
                            tableHistorial={this.state.tableHistorial}
                            onToggleTableHistorial={this.toggleTableHistorial}
                            tableClientes={this.state.tableCliente}
                            onToggleTableCliente={this.toggleTableCliente}
                        />


                        {this.state.isLoggedIn && this.state.registrationButtonVisible && (
                            <Button onClick={this.abrirModalRegistro} style={{
                                backgroundColor: '#3445B4',
                                position: 'absolute',
                                top: '20px',
                                right: '140px',
                                border: 'none'
                            }}>Registrarse</Button>
                        )}

                        {isLoggedIn ? (
                            <Button onClick={this.openSecondModal}
                                    style={{position: 'absolute', top: '20px', right: '20px'}}>
                                <i className="fas fa-user"></i>
                            </Button>
                        ) : (
                            <Button onClick={this.abrirModal} style={{
                                backgroundColor: '#3445B4',
                                position: 'absolute',
                                top: '20px',
                                right: '20px',
                                border: 'none'
                            }}> Mi Cuenta </Button>
                        )}
                    </div>
                </div>


                <Modal isOpen={abierto} style={modalStyles}>
                    <ModalHeader>
                        Iniciar Sesión
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="usuario">Usuario</Label>
                            <Input type="text" id="usuario" value={correo_electronico}
                                   onChange={this.handleUsernameChange} onBlur={this.handleEmailBlur}/>
                            {emailError && <span className="error-message">{emailError}</span>}
                        </FormGroup>
                        <FormGroup>
                            <Label for="password">Contraseña</Label>
                            <Input type={showPassword ? 'text' : 'password'} id="password" value={contrasena}
                                   onChange={this.handlePasswordChange} onBlur={this.handlePasswordBlur}/>
                            {passwordError && <span className="error-message">{passwordError}</span>}
                        </FormGroup>
                        <Button type="button"
                                onClick={this.handleTogglePasswordVisibility}>{showPassword ? 'Ocultar' : 'Mostrar'}</Button>
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={this.handleSubmit2} style={{backgroundColor: '#3445B4', border: 'none'}}>Iniciar
                            Sesión</Button>
                        <Alert color="success" isOpen={this.state.showSuccessAlert}>Usuario creado con éxito.</Alert>
                        <Button color="secondary" onClick={this.cerrarModal}>Cerrar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.registroAbierto} style={modalStyles}>
                    <ModalHeader>
                        Crear Cuenta
                    </ModalHeader>
                    <ModalBody>

                        <FormGroup>
                            <Label for="nombre">Nombre</Label>
                            <Input type="text" id="nombre" value={nombre} onChange={this.handleNombreChange}/>
                        </FormGroup>

                        <FormGroup>
                            <Label for="primer_apellido">Primer Apellido</Label>
                            <Input type="text" id="primer_apellido" value={primer_apellido}
                                   onChange={this.handlePrimerApellidoChange}/>
                        </FormGroup>

                        <FormGroup>
                            <Label for="segundo_apellido">Segundo Apellido</Label>
                            <Input type="text" id="segundo_apellido" value={segundo_apellido}
                                   onChange={this.handleSegundoApellidoChange}/>
                        </FormGroup>

                        <FormGroup>
                            <Label for="usuario">Correo</Label>
                            <Input type="text" id="usuario" value={correo_electronico}
                                   onChange={this.handleUsernameChange} onBlur={this.handleEmailBlur}/>
                            {emailError && <span className="error-message">{emailError}</span>}
                        </FormGroup>

                        <FormGroup>
                            <Label for="password">Contraseña</Label>
                            <Input type={showPassword ? 'text' : 'password'} id="password" value={contrasena}
                                   onChange={this.handlePasswordChange} onBlur={this.handlePasswordBlur}/>
                            {passwordError && <span className="error-message">{passwordError}</span>}
                        </FormGroup>
                        <FormGroup>
                            <Label for="confirmPassword">Confirmar Contraseña</Label>
                            <Input type={showPassword ? 'text' : 'password'} id="confirmPassword"
                                   value={confirmarContrasena} onChange={this.handleConfirmPasswordChange}
                                   onBlur={this.handleConfirmPasswordBlur}/>
                            {confirmPasswordError && <span className="error-message">{confirmPasswordError}</span>}
                        </FormGroup>

                        <FormGroup>
                            <Label for="num_colegiado">Número de Colegiado</Label>
                            <Input type="text" id="num_colegiado" value={num_colegiado} pattern="[0-9]*"
                                   onChange={this.handleNumColegiadoChange}/>
                        </FormGroup>

                        <FormGroup>
                            <Label for="exampleSelect">Seleccionar una opción</Label>
                            <Input type="select" name="select" id="exampleSelect" onChange={this.handleSelectChange}>
                                <option value=""></option>
                                {this.state.opcionesNivelAcceso.map((nivel) => (
                                    <option key={nivel.id} value={nivel.nombre}>{nivel.nombre}</option>
                                ))}
                            </Input>
                        </FormGroup>

                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={this.handleSubmit1} style={{backgroundColor: '#3445B4', border: 'none'}}>Crear
                            Cuenta</Button>

                        <Button onClick={this.cerrarModalRegistro}>Cerrar</Button>
                    </ModalFooter>
                    <div style={{textAlign: 'center', padding: '10px'}}>
                        <Alert color="success" isOpen={this.state.showSuccessAlert}>
                            Usuario creado con éxito.
                        </Alert>
                        <Alert color="danger" isOpen={this.state.showUserExistsAlert}>
                            El usuario ya existe.
                        </Alert>
                    </div>

                </Modal>

                <Modal isOpen={this.state.showSecondModal} style={modalStyles}>
                    <ModalHeader>
                        Mi Cuenta
                    </ModalHeader>
                    <ModalBody>
                        <p>Nombre de Usuario: {this.state.usuario}</p>
                    </ModalBody>
                    <ModalFooter>
                        {isLoggedIn && (
                            <Button onClick={this.handleLogout}>Cerrar Sesión</Button>
                        )}
                        <Button onClick={this.closeSecondModal}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
            </>
        )
    }
}

export default App;
